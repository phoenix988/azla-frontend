package templates

import ( 
	"fmt"
	"net/http"
	"io"
	"os")

func RenderProfileSection() (string, error) {
	 // Open the HTML file
    file, err := os.Open("templates/files/profile_section.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil

}

func RenderProfileSectionLang() (string, error) {
	 // Open the HTML file
    file, err := os.Open("templates/files/lang_section.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil
}

func RenderProfileSectionInfo() (string, error) {
	 // Open the HTML file
    file, err := os.Open("templates/files/info_section.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil
}


func RenderProfileSectionSecurity() (string, error) {
	 // Open the HTML file
    file, err := os.Open("templates/files/sec_section.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil

}

func RenderProfileSectionProg() (string, error) {
    // Open the HTML file
    file, err := os.Open("templates/files/prog_section.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil
}

func RenderMessageStr(mess string) string{
	htmlStr := fmt.Sprintf("<div id='popupMessage' class='success-message'>" + 
	"<div class='close-button' id='closeButton'>X</div>%s"+
	"</div>", mess)

	return htmlStr
}


func RenderProfileLangList() string {
	return fmt.Sprintf(`
	<div id="languages" class="language-card-container">
	{{ range $key, $value := .Progress.Data }}
	<div class="row">
	<div class="col">
	<!-- Default switch -->
	<div class="language-card">
	<label class="custom-checkbox">
	<input type="checkbox" id="{{$key}}" name="{{$key}}">
	<span class="checkmark"></span>
	{{$key}}
	</label>
	</div>
	</div>
	</div>
	`)
}

func RenderLoginAgainPopUp() string{
	return `
	<div id="overlay" class="overlay">
	<div id="cc" class="centered-container">
	<div class="content">
	<span id="closeButton" class="close-button-alt">&times;</span>
	<form hx-post="/validate/login/email" hx-target="#message">
	<h2>Confirm your identity </h2>
	<hr class="fancy">
	<br><div>Please sign in again</div><br>
	<div
	class="wrap-input100 validate-input"
	data-validate="Valid email is required: ex@abc.xyz"
	id="infoId"
	>
	<input
	class="input100"
	type="text"
	name="email"
	id="validateEmail"
	placeholder="Email"
	/>
	<span class="focus-input100"></span>
	<span class="symbol-input100">
	<i class="fa fa-envelope" aria-hidden="true"></i>
	</span>
	</div>
	<div
	class="wrap-input100 validate-input"
	data-validate="Password is required"
	>
	<input
	class="input100"
	type="password"
	name="pass"
	id="password"
	placeholder="Password"
	/>

	<input
	class="input100"
	type="email"
	name="newEmail"
	value="{{.Email}}"
	style="display: none"
	/>

	<span class="focus-input100"></span>
	<span class="symbol-input100">
	<i class="fa fa-lock" aria-hidden="true"></i>
	</span>
	</div>
	<button id="validateLogin" type="submit" class="pushable margin-top-big">
	<span class="d-shadow"></span>
	<span class="edge"></span>
	<span class="front">Login</span>
	</button>
	</form>
	</div>
	</div>
	</div>


	`
}

func RenderMessage(mess string) string{
	return fmt.Sprintf(`<form><input name="item" type="text" value="%s"/></form>`, mess)
}



func RenderAreYouSureProgress(mess, lang string) string{
	return fmt.Sprintf(`
	<div id="overlay" class="overlay">
	<div class="centered-container">
	<span id="closeButton" class="close-button-alt">&times;</span>
	<div class="content">
	<form hx-post="/leave" hx-target="#message">
	<span>Attention !! </span>
	<hr>
	<br><div>%s</div><br>
	</form>
	<div class="row">
	<div class="col-5">
	<form hx-target="#message" hx-post="/dialog/progress/yes" hx-trigger="click">
	<button id="yesButton" type="submit" class="btn-primary">Yes</button>
	<input name="item" type="text" value="%s" style="display: none;"/>
	</form>
	</div>
	<div class="col-6">
	<button type="submit" class="btn-sec">No</button>
	</div>
	</div>
	<hr class="fancy">
	</div>
	</div>
	</div>

	`, mess, lang)

}

func RenderAreYouSure(mess, lang string) (string, error){
    // Open the HTML file
    file, err := os.Open("templates/files/popup/exit.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil


	//return fmt.Sprintf(`
	//<div id="overlay" class="overlay">
	//<div id="cc" class="centered-container">
	//<div class="content">
	//<span id="closeButton" class="close-button-alt">&times;</span>
	//<form hx-post="/leave" hx-target="#message">
	//<h3>Attention !! </h3>
	//<hr>
	//<br><h3>%s</h3><br>
	//<br>
	//</form>
	//<div class="row">
	//<div class="col-6">
	//<form hx-post="/redirect/stage" hx-trigger="click">
	//<button type="submit" class="btn-primary">Yes</button>
	//<input name="item" type="text" value="%s" style="display: none;"/>
	//</form>
	//<br>
	//</div>
	//<div class="col-6">
	//<form hx-target="#message" hx-post="/dialog/quiz/no" hx-trigger="click">
	//<button type="submit" class="btn-primary">No</button>
	//</form>
	//</div>
	//</div>
	//</div>
	//</div>
	//</div>

	//`, mess, lang)
}

func RenderSuccessMessage(w http.ResponseWriter, mess string) (int, error) {
		htmlStr := fmt.Sprintf("<div class='success-message'>" + 
		"<div class='close-button' id='closeButton'>X</div>%s"+
		"</div>", mess)
		// Write the HTML content to the response
		w.Header().Set("Content-Type", "text/html")
		return fmt.Fprintf(w, htmlStr)
}


func RenderFailedMessage(w http.ResponseWriter, mess string) (int, error) {
		htmlStr := fmt.Sprintf("<div class='failed-message'>" + 
		"<div class='close-button' id='closeButton'>X</div>%s"+
		"</div>", mess)
		// Write the HTML content to the response
		w.Header().Set("Content-Type", "text/html")
		return fmt.Fprintf(w, htmlStr)
}

func RenderStages() string {
	return fmt.Sprintf(`
	<!-- Carousel -->
	<section class="carousel">
	<div class="reel">
	<article hx-get="/update/stage/1" hx-trigger="load" hx-target="#Stage1">
	<a href="#stage1" class="image featured"
	><img src="/assets/pic01.jpg" alt=""
	/></a>
	<header>
	<h3>
	<a id="Stage1" class="section" href="#stage1"
	>Stage 1</a
	>
	</h3>
	</header>
	<p>basic sentence structures and greetings</p>
	</article>

	<article hx-get="/update/stage/2" hx-trigger="load" hx-target="#Stage2">
	<a href="#stage2" class="image featured"
	><img src="/assets/pic02.jpg" alt=""
	/></a>
	<header>
	<h3><a id="Stage2" class="section" href="#stage2">Stage 2</a></h3>
	</header>
	<p>Order in a resturant, cafe and shops</p>
	</article>

	<article hx-get="/update/stage/3" hx-trigger="load" hx-target="#Stage3" >
	<a href="#stage3" class="image featured"
	><img src="/assets/pic03.jpg" alt=""
	/></a>
	<header>
	<h3><a id="Stage3" class="section" href="#stage3">Stage 3</a>
	</h3>
	</header>
	<p>Describe your job</p>
	</article>
	</div>
	</section>

	`)
}

func RenderHeader() (string, error) {
	 // Open the HTML file
    file, err := os.Open("templates/files/header.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil
}

func RenderFooter() (string, error) {
	// Open the HTML file
    file, err := os.Open("templates/files/footer.html")
    if err != nil {
        return "", err
    }
    defer file.Close()

    // Read the file content
    content, err := io.ReadAll(file)
    if err != nil {
        return "", err
    }
    return string(content), nil
}
