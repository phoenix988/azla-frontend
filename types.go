package main

type Accounts struct {
	Update Update
}

type Image struct {
	File string `json:"file"`
}


type Sending struct {
	Send Send
	ReturnRequests ReturnSend
}

type CreateUserRequest struct {
	FirstName       string
	LastName        string
	Username        string
	Password        string
	Email           string
	PasswordConfirm string
}


type NewSessionRequest struct {
	MaxAmountWords string
	Question       string
	WordList       string
	ID             string
}

type NewReviewRequest struct {
	Lesson string `json:"lesson"`
	NumberOfQuestions string `json:"numberOfQuestions"`
	Words []string `json:"words"`
}

type CurrentWordRequest struct {
	ID       string
	Question string
	WordList string
}

type QuizSession struct {
	CurrentWord    string
	CurrentCorrect string
	CurrentImage   string
	CurrentAnswer  []string
	AllWords       []string
	CurrentQuestion int
	SelectedLanguage string
}


type PageData struct {
	IsUserNew bool
	Configure bool
	Message string
	Lang string
}

type Result struct {
	CorrectAnswers   int
	IncorrectAnswers int
}

type LoginRequest struct {
	IsSuccess string
	UserName string `json:"userName"`
	Password string `json:"password"`
}

type WordList struct {
	 Name string `json:"wordList"`
	 Count string `json:"count"`
}

type profileSectionData struct {
	Language string
	Security string
	UserInformation string
	Progression string
	Email string
	UserName string
	FullName string
	Info bool
	Sec bool
	Lang bool
	Prog bool
	Progress *ProgressData
	Checked bool
	PreferLang string
}


type UpdateUserData struct {
	Email string `json:"email"`
	UserName string `json:"username"`
	FirstName string `json:"firstname"`
	LastName string `json:"lastname"`
}


type UserData struct {
	Email string `json:"email"`
	UserName string `json:"username"`
	FirstName string `json:"firstname"`
	LastName string `json:"lastname"`
	FullName string `json:"fullname"`
}


type ProgressData struct {
	Data map[string]*Progress
}

type Progress struct {
	Language string
	Level map[string]interface{}
}


type Default struct {
	Language string
}

var def = Default{
	Language: "azerbajani",
}

