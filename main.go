package main

import (
	"github.com/gorilla/sessions"
	"net/http"
)

func init() {
	// Create a route to serve static files
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.Handle("/theme/", http.StripPrefix("/theme/", http.FileServer(http.Dir("theme"))))
	http.Handle("/static/css", http.StripPrefix("/static/css", http.FileServer(http.Dir("static/css"))))
	http.Handle("/src/", http.StripPrefix("/src/", http.FileServer(http.Dir("src"))))

	store.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   3600 * 8, // 8 hours
		HttpOnly: true,
	}

}

func main() {
	port := ":8082"
	requests, _ := NewRequests()
	server := NewWebServer(port, requests)
	server.Run()
}


