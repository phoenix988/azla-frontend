package main

import (
	"azla_go_learning_front/templates"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
	"bytes"
)

var static = "static" // directory for html templates
var staticPages = "static/pages"

type WebServer struct {
	listenAddr string
	requests   Requests
}

func NewWebServer(listenAddr string, requests Requests) *WebServer {
	return &WebServer{
		listenAddr: listenAddr,
		requests:   requests,
	}

}

func (s *WebServer) Run() {
	router, _ := CreateRoutes(s)

	// logs
	log.Println("Frontend server running on port: ", s.listenAddr)
	
	// Start the server
	http.ListenAndServe(s.listenAddr, router)
}

func (s *WebServer) handleMenuNoId(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else {
		http.Redirect(w, r, id+"/menu", http.StatusSeeOther)
	}

	return nil
}

func (s *WebServer) handleMenuId(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	url := mux.Vars(r)["id"]

	if r.URL.Path == "/create_account" {
		return s.handleCreateAccountMenu(w, r)
	} else if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else {
		switch r.URL.Path {
		case "/profile":
			http.Redirect(w, r, "/"+id+"/profile", http.StatusSeeOther)
			
		case "/menu":
			http.Redirect(w, r, "/"+id+"/menu", http.StatusSeeOther)
		}

	}

	checkIfNumber,_  := strconv.Atoi(url) 
	if checkIfNumber == 0  {
		return nil
	} else {
		http.Redirect(w, r, "/"+id+"/menu", http.StatusSeeOther)

	}

	return nil
}

func (s *WebServer) handleMenu(w http.ResponseWriter, r *http.Request) error {
	// Retrieve the session
	err := LoggedIn(r, w)
	if err == nil {
		id := getCookie(r, "id")
		token, _ := getToken(r)
		userSettings, _ := s.requests.GetSettings(id, token)
		progress, _ := s.requests.GetAllProgress(id, r)

		isNew, _ := userSettings["isNewAccount"].(bool)

		pageData := &PageData{
			IsUserNew: isNew,
			Message: "You need to choose language",
		}

		if len(progress) == 0 {
			pageData.Configure = true
		} else {
			pageData.Configure = false
		}

		return displayTemplateWithHeaderFooter(w, static + "/main_page.html", pageData)
	}

	return nil
}

func (s *WebServer) handleLastStageMenu(w http.ResponseWriter, r *http.Request) error {
	session, _ := store.Get(r, "session-name")

	return WriteJSON(w, http.StatusOK, session.Values["lastPath"])
}

func (s *WebServer) handleStageMenu(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	usedId := mux.Vars(r)["id"]
	var stage string

	token, err := getToken(r)
	if err != nil {
		return err
	}
	settings, err := s.requests.GetSettings(id, token)
	if err != nil {
		http.Redirect(w, r, "/"+id+"/page_not_found", http.StatusSeeOther)
	}

	language, _ := settings["defaultLanguage"].(string)

	if language == "" {
		language = def.Language
	}

	session, err := store.Get(r, "session-name")
	if err != nil {
		return err
	}

	if r.URL.Path != "/get/menu/stage" {
		session.Values["path"] = r.URL.Path
		session.Save(r, w)
	}

	lastPath, ok := session.Values["path"].(string)

	if ok {
		stage = strings.Split(lastPath, "/")[3]
	}

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else if id != usedId {
		http.Redirect(w, r, "/"+id+"/menu/"+language+"/"+stage, http.StatusSeeOther)
	} else {
		tmpl, _ := template.ParseFiles(static + "/stages.html")

		// Create a buffer to hold the template output
		var tmplBuffer bytes.Buffer

		// Execute the template and write its output to the buffer
		err := tmpl.Execute(&tmplBuffer, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return err
		}

		footer, err := templates.RenderFooter()

		// Combine the template output with the HTML string
		combinedOutput := tmplBuffer.String() + footer

		// Write the combined output to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprint(w, combinedOutput)
		return nil
	}

	return nil
}

func (s *WebServer) handleStageMenuRedirect(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	token, err := getToken(r)
	if err != nil {
		return err
	}
	stage := mux.Vars(r)["stage"]

	session, err := store.Get(r, "session-name")
	if err != nil {
		return err
	}

	session.Values["stage"] = stage
	session.Save(r, w)
	settings, err := s.requests.GetSettings(id, token)
	language, _ := settings["defaultLanguage"].(string)

	if language == "" {
		language = def.Language
	}

	http.Redirect(w, r, "/"+id+"/menu/"+language+"/"+stage, http.StatusSeeOther)

	return nil
}

func (s *WebServer) handleReviewMistakes(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	usedId := mux.Vars(r)["id"]

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else if id != usedId {
		http.Redirect(w, r, "/"+id+"/menu/aze", http.StatusSeeOther)
	} else {
		tmpl, _ := template.ParseFiles(staticPages + "/review_mistakes.html")
		tmpl.Execute(w, nil)
	}

	return nil
}

func (s *WebServer) handleProgressRedirect(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	http.Redirect(w, r, "/"+id+"/progress/aze", http.StatusSeeOther)
	return nil
}

func (s *WebServer) handleProgressMenu(w http.ResponseWriter, r *http.Request) error {
	session, _ := store.Get(r, "session-name")

	session.Values["lastPath"] = "progress"
	session.Save(r, w)

	tmpl, _ := template.ParseFiles(static + "/profile/progress.html")

	// Create a buffer to hold the template output
	var tmplBuffer bytes.Buffer

	// Execute the template and write its output to the buffer
	err := tmpl.Execute(&tmplBuffer, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return err
	}

	// Combine the template output with the HTML string
	combinedOutput := tmplBuffer.String()

	// Write the combined output to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, combinedOutput)
	return nil
}

func (s *WebServer) handleLoggedInUser(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	userData := s.GetUserInformation(id, r, w)

	htmlStr := fmt.Sprintf("%s", userData.FullName)

	// Write the HTML content to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, htmlStr)

	return nil
}

// update the mistakes list on reload using htmx
func (s *WebServer) handleUpdateMistakes(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	token, _ := getToken(r)
	sessions, err := s.requests.GetAllSessionsForUser(id, token)
	session, _ := store.Get(r, "session-name")

	if err != nil {
		return err
	}

	yourMistakes := getUniqueMistakesFromSession(sessions)

	// Convert yourMistakes array to a string
	var iconHTML strings.Builder

	iconHTML.WriteString(`
          <h2 class="fancy-text">Review Recent Mistakes</h2>
          <div style="padding: 30px;">
            <button hx-post="/quiz/review/start" hx-trigger="click" id="reviewButton" type="submit" class="pushable margin-top-big">
              <span class="d-shadow"></span>
              <span class="edge"></span> 
              <span class="front">Start</span>
            </button>
          </div>`)

	iconHTML.WriteString("<p id='title' class='fancy-text'>Mistakes " + strconv.Itoa(len(yourMistakes)) + "</p>")
	iconHTML.WriteString("<br>") // Add line break between each element
	for _, mistake := range yourMistakes {
		iconHTML.WriteString("<div class='text-container'<div class='mistake-entry'>Translate: " + mistake + "</div></div>")
	}

	session.Values["mistakes"] = len(yourMistakes) // amount of mnistakes
	session.Values["mistakesWord"] = yourMistakes  // all mistakes in word
	session.Values["lastPath"] = "review"
	session.Save(r, w)

	time.Sleep(0 * time.Second)

	// Write the HTML content to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, "%s", iconHTML.String())

	return nil
}

func (s *WebServer) handleStartMainQuiz(w http.ResponseWriter, r *http.Request) error {
	fmt.Println("test")
	return nil
}

// start the review session
func (s *WebServer) handleStartReviewRedir(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	session, err := store.Get(r, "session-name")
	if err != nil {
		return err
	}

	sesId, ok := session.Values["sesId"].(string)

	if ok {
		http.Redirect(w, r, "/"+id+"/quiz/"+sesId, http.StatusSeeOther)
	} else {
		http.Redirect(w, r, "/"+id+"/page_not_found", http.StatusSeeOther)
	}

	return nil
}

// redirect to review mistakes menu
func (s *WebServer) handleReviewRedirect(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")

	http.Redirect(w, r, "/"+id+"/menu/aze/review/mistakes", http.StatusSeeOther)
	return nil
}

func (s *WebServer) handleStartReview(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	token, err := getToken(r)
	if err != nil {
		return err
	}

	session, err := store.Get(r, "session-name")

	data := map[string]interface{}{
		"lesson":            "0",
		"numberOfQuestions": session.Values["mistakes"].(int),
		"words":             session.Values["mistakesWord"].([]string),
	}

	start, err := s.requests.StartNewReviewSession(id, token, data)
	sentences, _ := start["sentences"].(map[string]interface{})
	question, _ := sentences["0"].(map[string]interface{})
	sessionId := question["id"].(float64)
	sesId := strconv.Itoa(int(sessionId))

	session.Values["sesId"] = sesId
	session.Save(r, w)

	url := "/" + id + "/quiz/" + sesId

	hxredirect(w, url)

	return nil
}

// redirect to profile
func (s *WebServer) handleProfileRedirect(w http.ResponseWriter, r *http.Request) error {
	http.Redirect(w, r, "/1/profile", http.StatusSeeOther)
	return nil
}

func (s *WebServer) handleProfile(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	usedId := mux.Vars(r)["id"]
	session, _ := store.Get(r, "session-name")
	section, _ := session.Values["section"].(string)
	selectedSection := mux.Vars(r)["section"]

	session.Values["selectAll"] = false
	session.Save(r, w)

	// select a specific section in profile settings
	if selectedSection != "" {
		session.Values["section"] = selectedSection
		session.Save(r, w)
		http.Redirect(w, r, "/"+id+"/profile", http.StatusSeeOther)
	}

	userData := s.GetUserInformation(id, r, w)
	progress := s.GetUserProgress(id, r, w)
	profile := profileSectionData{}

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else if id != usedId { // if you dont use the correct id
		http.Redirect(w, r, "/"+id+"/profile", http.StatusSeeOther)
	} else {
		switch section {
		case "info":
			profile.Info = true
			profile.UserInformation = "user-container"
			profile.Email = userData.Email
			profile.UserName = userData.UserName
			profile.FullName = userData.FullName
		case "sec":
			profile.Sec = true
			profile.Security = "user-container"
			profile.Email = userData.Email
			profile.UserName = userData.UserName
			profile.FullName = userData.FullName

		case "language":
			profile.Lang = true
			profile.Language = "user-container"
			profile.Email = userData.Email
			profile.UserName = userData.UserName
			profile.FullName = userData.FullName

		case "progression":
			profile.Prog = true
			profile.Progression = "user-container"
			profile.Email = userData.Email
			profile.UserName = userData.UserName
			profile.FullName = userData.FullName
			profile.Progress = progress
		default:
			profile.Info = true
			profile.UserInformation = "user-container"
			profile.Email = userData.Email
			profile.UserName = userData.UserName
			profile.FullName = userData.FullName
		}

		displayTemplateWithHeaderFooter(w, static + "/profile.html", profile)

	}

	return nil
}

func (s *WebServer) handleUpdates(w http.ResponseWriter, r *http.Request) error {
	action := mux.Vars(r)["action"]
	switch action {
	case "password":
		return s.handleUpdatePassword(w, r)
	case "settings":
		return s.handleUpdateSettings(w, r)
	case "progress":
		return s.handleUpdateProgress(w, r)
	case "dprogress":
		return s.handleDeleteProgress(w, r)
	case "username":
		return s.handleLoggedInUser(w, r)
	case "darkmode":
		return s.handleUpdateDarkMode(w, r)
	case "languages":
		return s.handleActivateAllLanguages(w, r)
	case "load_stages":
		return s.handleLoadStages(w, r)
	case "default_language":
		return s.handleUpdateDefaultLanguage(w, r)
	case "configuration_languages":
		return s.handleUpdateAvailableLang(w, r)

	case "choosen_language":
		return s.handleUpdateAvailableLangByLang(w, r)
	default:
		return fmt.Errorf("%s not supported", action)
	}

}

func (s *WebServer) handleUpdateDefaultLanguage(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	progress := s.GetUserProgress(id, r, w)

	for lang := range progress.Data {
		form := r.FormValue(lang)
		if form == "on" {
			s.requests.UpdateAccountSettings(r, "language", strings.ToLower(lang))
			fmt.Fprint(w, lang)
		}
	}

	return nil
}

func (s *WebServer) handleActivateAllLanguages(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")

	progress := s.GetUserProgress(id, r, w)
	session, _ := store.Get(r, "session-name")
	selectAll, _ := session.Values["selectAll"].(bool)

	// Convert yourMistakes array to a string
	var html strings.Builder

	// Create a slice to store the language strings
	languages := make([]string, 0)

	for _, lang := range progress.Data {
		languages = append(languages, lang.Language)
	}

	sort.Strings(languages)

	for _, lang := range languages {
		html.WriteString("" +
			"<div class='row'>" +
			"<div class='col'>" +
			"<div class='language-card'>" +
			"<label class='custom-checkbox'>")
		if !selectAll {
			html.WriteString("" +
				"<input type='checkbox' id=" + lang + "name=" + lang + " checked>")
		} else {

			html.WriteString("" +
				"<input type='checkbox' id=" + lang + "name=" + lang + ">")
		}

		html.WriteString("" +
			"<span class='checkmark'></span>" +
			"" + lang +
			"</label>" +
			"</div>" +
			"</div>" +
			"</div>")
	}

	if !selectAll {
		session.Values["selectAll"] = true
		session.Save(r, w)
	} else {
		session.Values["selectAll"] = false
		session.Save(r, w)
	}

	// Write the HTML content to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, "%s", html.String())
	return nil

}

func (s *WebServer) handleUpdateDarkMode(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	token, _ := getToken(r)
	settings, _ := s.requests.GetSettings(id, token)

	darkmode, _ := settings["darkmode"].(bool)

	if darkmode {
		s.requests.UpdateAccountSettings(r, "dark_mode", "false")

	} else {
		s.requests.UpdateAccountSettings(r, "dark_mode", "true")

	}

	return nil

}

func (s *WebServer) handleUpdateSettings(w http.ResponseWriter, r *http.Request) error {
	r.ParseForm()
	fullname := r.FormValue("firstname")
	splitName := strings.Split(fullname, " ")

	id := getCookie(r, "id")
	token, _ := getToken(r)
	userData := s.GetUserInformation(id, r, w) // current user info
	updateData := UpdateUserData{              // data to update with
		Email:     r.FormValue("email"),
		UserName:  r.FormValue("username"),
		FirstName: splitName[0],
		LastName:  splitName[1],
	}
	settingUpdated := false
	settingMessage := map[string]bool{}

	doesEmailExist, _ := s.requests.CheckIfEmailIsOk(updateData.Email)
	doesUserExist, _ := s.requests.CheckIfUserNameIsOk(updateData.UserName)

	if updateData.Email != userData.Email && doesEmailExist {
		templates.RenderFailedMessage(w, "email already exist")

		return nil
	}

	if updateData.UserName != userData.UserName && doesUserExist {
		templates.RenderFailedMessage(w, "username already exist")

		return nil
	}

	if updateData.UserName != userData.UserName {
		settingUpdated = true
		settingMessage["username"] = true
		account.Update.UserName(s, id, token, updateData.UserName)
	}

	if fullname != userData.FullName {
		if splitName[0] != "" && splitName[1] != "" {
			account.Update.FullName(s, id, token, splitName)
		}
		settingUpdated = true
	}

	if updateData.Email != userData.Email {
		settingUpdated = true
		settingMessage["email"] = true

		html := templates.RenderLoginAgainPopUp()

		tmpl, err := template.New("profileSection").Parse(html)
		if err != nil {
			http.Error(w, "Unable to parse template", http.StatusInternalServerError)
			return nil
		}
		if err := tmpl.Execute(w, updateData); err != nil {
			http.Error(w, "Unable to execute template", http.StatusInternalServerError)
		}

		return nil
	}

	if settingUpdated {
		templates.RenderSuccessMessage(w, "Saved changes")
	}

	return nil
}

func (s *WebServer) handleUpdatePassword(w http.ResponseWriter, r *http.Request) error {
	r.ParseForm()

	id := getCookie(r, "id")
	token, _ := getToken(r)
	userData := s.GetUserInformation(id, r, w)
	password := r.FormValue("pass")
	confirmPassword := r.FormValue("passwordConfirm")
	currentPassword := r.FormValue("passwordCurrent")

	loginAttempt, _ := loginUser(s, userData.Email, currentPassword)

	if len(loginAttempt) == 0 {
		htmlStr := fmt.Sprintf("<div class='failed-message'>" +
			"<div class='close-button' id='closeButton'>X</div>incorrect password</div>")

		// Write the HTML content to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprintf(w, htmlStr)
		// failes to authenticate passwords
		return nil
	}

	if password != confirmPassword {
		htmlStr := fmt.Sprintf("<div class='failed-message'>" +
			"<div class='close-button' id='closeButton'>X</div>new password does not match</div>")

		// Write the HTML content to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprintf(w, htmlStr)
		// password doesnt match
		return nil
	}

	requestBody := map[string]string{"currentPassword": currentPassword, "password": password, "confirmPassword": confirmPassword}
	_, _ = s.requests.UpdateSettings(id, token, "password", requestBody)

	htmlStr := fmt.Sprintf("<div class='success-message'>" +
		"<div class='close-button' id='closeButton'>X</div>password change successful</div>")

	// Write the HTML content to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, htmlStr)

	return nil
}

func (s *WebServer) handleExitQuiz(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	token, _ := getToken(r)
	settings, _ := s.requests.GetSettings(id, token)

	language, _ := settings["defaultLanguage"].(string)
	if language == "" {
		language = "azerbajani"
	}

	session, _ := store.Get(r, "session-name")
	sessionId, _ := session.Values["sessionId"].(string)
	s.requests.UpdateSessionComplete(r, sessionId)

	hxredirect(w, "/"+id+"/menu/"+language+"/Stage1")
	return nil
}

func (s *WebServer) handleYesResetProgress(w http.ResponseWriter, r *http.Request) error {

	return nil
}

func (s *WebServer) handleYesDeleteProgress(w http.ResponseWriter, r *http.Request) error {
	r.ParseForm()
	id := getCookie(r, "id")
	token, err := getToken(r)
	if err != nil {
		return err
	}
	lang := r.FormValue("item")

	_, err = s.requests.DeleteProgress(id, strings.ToLower(lang), token)
	if err != nil {
		return err
	}

	htmlStr := templates.RenderMessageStr("progress for " + lang + " is removed")

	progress := s.GetUserProgress(id, r, w)

	profile := profileSectionData{
		Progression: "user-container",
		Progress:    progress,
	}

	_ = showProfileSection(r, w, htmlStr, "progression", profile)

	hxredirect(w, "/"+id+"/profile")

	return nil
}

func (s *WebServer) handleUpdateProgress(w http.ResponseWriter, r *http.Request) error {
	r.ParseForm()
	id := getCookie(r, "id")
	//token, _ := getToken(r)

	progress := s.GetUserProgress(id, r, w)

	deleteForm := r.FormValue("delete")
	resetForm := r.FormValue("reset")

	if resetForm == "on" && deleteForm == "on" {
		return nil
	}

	profile := profileSectionData{
		Progression: "user-container",
		Progress:    progress,
	}

	for lang := range progress.Data {
		form := r.FormValue(lang)
		if form == "on" {
			if deleteForm == "on" {
				render := templates.RenderAreYouSureProgress("Are you sure you want to delete progress for "+lang+"?",
					strings.ToLower(lang))
				htmlStr := fmt.Sprintf("%s", render)
				// Write the HTML content to the response
				w.Header().Set("Content-Type", "text/html")
				fmt.Fprintf(w, htmlStr)

				return nil
			}

			if resetForm == "on" {
				render, _ := templates.RenderAreYouSure("Are you sure you want to reset progress for "+lang+"?",
					strings.ToLower(lang))

				htmlStr := fmt.Sprintf("%s", render)
				// Write the HTML content to the response
				w.Header().Set("Content-Type", "text/html")
				fmt.Fprintf(w, htmlStr)

				return nil
				//request, err := s.requests.ResetProgress(id, strings.ToLower(lang), token)
				//if err != nil {
				//	return err
				//}
				//fmt.Println(request)
			}
		}
	}

	lang := templates.RenderProfileLangList()
	html := lang

	_ = showProfileSection(r, w, html, "progression", profile)

	return nil
}

func (s *WebServer) handleDeleteProgress(w http.ResponseWriter, r *http.Request) error {
	fmt.Println("e")
	return nil
}

func (s *WebServer) handleListImages(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")

	files, err := listFiles("user_data/" + id)

	if err != nil {
		return err
	}

	return WriteJSON(w, http.StatusOK, files)
}


func (s *WebServer) handleCheckIfDirExist(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")

	token, _ := getToken(r)
	exist, err := CheckIfDirExist("/path")

	if err != nil {
		return err
	}

	settings, _ := s.requests.GetSettings(id, token)

	fmt.Print(settings)

	return WriteJSON(w, http.StatusOK, exist)
}

func (s *WebServer) handleUploadImageTemp(w http.ResponseWriter, r *http.Request) error {
	UploadImage(r, w, "temp")
	return nil
}

func (s *WebServer) handleUploadImage(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	filePath, _ := UploadImage(r, w, id)
	// write to database
	s.requests.UpdateAccountProfilePicture(r, "picture", "./"+filePath)

	return nil
}

func (s *WebServer) handleGoToProfileSection(w http.ResponseWriter, r *http.Request) error {
	section := mux.Vars(r)["section"]
	id := getCookie(r, "id")
	session, _ := store.Get(r, "session-name")
	session.Values["section"] = section
	session.Save(r, w)

	hxredirect(w, "/"+id+"/profile")

	return nil
}

func (s *WebServer) handleProfileSections(w http.ResponseWriter, r *http.Request) error {
	section := mux.Vars(r)["section"]
	id := getCookie(r, "id")
	profileData := s.GetUserInformation(id, r, w)

	switch section {
	case "language":
		session, _ := store.Get(r, "session-name")
		session.Values["section"] = section
		session.Save(r, w)
		s.handleConfigurationMenu(w, r)

	case "info":
		profile := profileSectionData{
			UserInformation: "user-container",
			Email:           profileData.Email,
			UserName:        profileData.UserName,
			FullName:        profileData.FullName,
		}

		session, _ := store.Get(r, "session-name")
		session.Values["section"] = section
		session.Save(r, w)

		tmpl, _ := template.ParseFiles(static + "/profile_info.html")
		// Create a buffer to hold the template output
		var tmplBuffer bytes.Buffer

		// Execute the template and write its output to the buffer
		err := tmpl.Execute(&tmplBuffer, profile)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return err
		}

		// Combine the template output with the HTML string
		combinedOutput := tmplBuffer.String()

		// Write the combined output to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprint(w, combinedOutput)
		return nil


	case "sec":
		profile := profileSectionData{
			Security: "user-container",
		}

		sec,_ := templates.RenderProfileSectionSecurity()
		html := sec

		_ = showProfileSection(r, w, html, section, profile)


	case "progression":
		progress := s.GetUserProgress(id, r, w)
		profile := profileSectionData{
			Progression: "user-container",
			Progress:    progress,
		}

		sec,_ := templates.RenderProfileSectionProg()
		html := sec

		_ = showProfileSection(r, w, html, section, profile)

	}

	return nil
}


func (s *WebServer) handleValidateExit(w http.ResponseWriter, r *http.Request) error {
	html, _ := templates.RenderAreYouSure("", "")

	data := PageData{
		Message: "Do you want to leave ?",
		Lang: "",
	}

	tmpl, err := template.New("profileSection").Parse(html)
	if err != nil {
		http.Error(w, "Unable to parse template", http.StatusInternalServerError)
		return nil
	}
	if err := tmpl.Execute(w, data); err != nil {
		http.Error(w, "Unable to execute template", http.StatusInternalServerError)
	}

	return nil
}

func (s *WebServer) handleValidate(w http.ResponseWriter, r *http.Request) error {
	method := mux.Vars(r)["method"]

	switch method {

	case "exit":
		return s.handleValidateExit(w, r)
	case "email":

		r.ParseForm()
		email := r.FormValue("email")
		password := r.FormValue("pass")
		newEmail := r.FormValue("newEmail")
		id := getCookie(r, "id")
		token, _ := getToken(r)

		attempt, _ := loginUser(s, email, password)

		if len(attempt) == 0 {
			templates.RenderFailedMessage(w, "failed to change email")
			return nil
		}

		check := account.Update.Email(s, id, token, newEmail)

		if check {
			templates.RenderSuccessMessage(w, "email already exist")
		}

		templates.RenderSuccessMessage(w, "email changed")

		return nil

	default:
		return nil

	}
}

func (s *WebServer) handleLoginMenu(w http.ResponseWriter, r *http.Request) error {
	session, _ := store.Get(r, "session-name")
	userID, ok := session.Values["userId"].(float64)
	id := strconv.Itoa(int(userID)) // convert to string
	if !ok || userID == 0 {
		tmpl, _ := template.ParseFiles(static + "/login.html")
		return tmpl.Execute(w, nil)
	} else {
		http.Redirect(w, r, id+"/menu", http.StatusSeeOther)
	}

	return nil
}


func (s *WebServer) handleLoginRedirect(w http.ResponseWriter, r *http.Request) error {
	if !AreYouLoggedIn(r) {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	}

	id := getCookie(r, "id")

	session, err := store.Get(r, "session-name")
	if err != nil {
		return fmt.Errorf("failed to retrieve session")
	}
	token := r.Header.Get("Authorization")
	session.Values["token"] = token
	err = session.Save(r, w)
	if err != nil {
		return fmt.Errorf("failed to save session: %v", err)
	}

	err = s.CheckIfUserIsNew(r, w, token)
	if err != nil {
		http.Redirect(w, r, "/"+id+"/account_configuration", http.StatusSeeOther)
	} else {
		http.Redirect(w, r, "/"+id+"/menu", http.StatusSeeOther)
	}

	return nil
}

// Sign in request to the backend server
func (s *WebServer) handleSignIn(w http.ResponseWriter, r *http.Request) error {
	username := r.Header.Get("username")
	password := r.Header.Get("password")
	session, err := store.Get(r, "session-name")
	if err != nil {
		return fmt.Errorf("failed to retrieve session")
	}

	token := r.Header.Get("Authorization")
	session.Values["token"] = token

	loginAttempt, _ := loginUser(s, username, password)

	if len(loginAttempt) == 0 {
		return fmt.Errorf("failed")
	}

	profile, _ := s.requests.GetUserProfile(loginAttempt["email"].(string), loginAttempt["token"].(string))

	session.Values["userId"] = profile["id"]
	// Save the session
	err = session.Save(r, w)
	if err != nil {
		return fmt.Errorf("failed to save session: %v", err)
	}

	fmt.Printf("Session values after saving: %v\n", session.Values)

	return nil

}

func (s *WebServer) handleSignOut(w http.ResponseWriter, r *http.Request) error {
	session, err := store.Get(r, "session-name")
	delete(session.Values, "userId")
	delete(session.Values, "token")
	session.Save(r, w)
	if err != nil {
		return err
	}

	tmpl, _ := template.ParseFiles(staticPages + "/logout.html")
	//tmpl, _ := template.New("").ParseFiles(static + "/base.html", static + "/login.html")
	return tmpl.Execute(w, nil)
}

func (s *WebServer) handleLogout(w http.ResponseWriter, r *http.Request) error {
	// Revoke authentication
	removeSession(w, r)
	http.Redirect(w, r, "/login", http.StatusSeeOther)

	return nil
}

func (*WebServer) handleCreateAccountMenu(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	
	if id == "" {
		tmpl, _ := template.ParseFiles(static + "/create_account.html")
		tmpl.Execute(w, nil)
	} else {
		http.Redirect(w, r, "/"+id+"/menu", http.StatusSeeOther)

	}

	return nil
}

func (s *WebServer) handleStartQuiz(w http.ResponseWriter, r *http.Request) error {
	//lesson := r.Header.Get("lesson")serv

	return nil
}

func (s *WebServer) handleRunQuiz(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	usedId := mux.Vars(r)["id"]
	url := strings.Split(r.URL.Path, "/")
	session, err := store.Get(r, "session-name")
	if err != nil {
		return err
	}

	session.Values["sessionId"] = url[3]
	session.Save(r, w)

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else if id != usedId {
		http.Redirect(w, r, "/"+id+"/menu", http.StatusSeeOther)
	} else {
		tmpl, err := template.ParseFiles(static + "/quiz.html")

		// Create a buffer to hold the template output
		var tmplBuffer bytes.Buffer

		// Execute the template and write its output to the buffer
		err = tmpl.Execute(&tmplBuffer, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return err
		}

		footer, _ := templates.RenderFooter()

		// Combine the template output with the HTML string
		combinedOutput := tmplBuffer.String() + footer

		// Write the combined output to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprint(w, combinedOutput)
	}

	return nil
}

func (s *WebServer) handleLastQuestion(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else {

		tmpl, _ := template.ParseFiles(staticPages + "/last_question.html")

		// Create a buffer to hold the template output
		var tmplBuffer bytes.Buffer

		// Execute the template and write its output to the buffer
		err := tmpl.Execute(&tmplBuffer, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return err
		}

		footer,_  := templates.RenderFooter()

		// Combine the template output with the HTML string
		combinedOutput := tmplBuffer.String() + footer

		// Write the combined output to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprint(w, combinedOutput)
	}

	return nil
}

func (s *WebServer) handleNoHealth(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else {
		tmpl, _ := template.ParseFiles(staticPages + "/no_health.html")

		// Create a buffer to hold the template output
		var tmplBuffer bytes.Buffer

		// Execute the template and write its output to the buffer
		err := tmpl.Execute(&tmplBuffer, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return err
		}

		footer, _ := templates.RenderFooter()

		// Combine the template output with the HTML string
		combinedOutput := tmplBuffer.String() + footer

		// Write the combined output to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprint(w, combinedOutput)
	}

	return nil
}

func (s *WebServer) handleRedirect(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else {
		http.Redirect(w, r, "/"+id+"/menu", http.StatusSeeOther)
	}

	return nil
}

func (s *WebServer) handleRequest(w http.ResponseWriter, r *http.Request) error {
	println(r.URL.Path)

	return nil
}

func (s *WebServer) handleNoData(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	usedId := mux.Vars(r)["id"]

	err := LoggedIn(r, w)
	if err != nil {
		return err
	}

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	} else if id != usedId {
		http.Redirect(w, r, "/"+id+"/page_not_found", http.StatusSeeOther)
	} else {
		return displayTemplateWithHeaderFooter(w, staticPages + "/page_not_found.html", nil)
	}

	return nil
}

func (s *WebServer) handleGetToken(w http.ResponseWriter, r *http.Request) error {
	token, err := getToken(r)
	if err != nil || token == "null" {
		return fmt.Errorf("access denied")
	}
	return WriteJSON(w, http.StatusOK, token)
}


func (s *WebServer) handleGetImage(w http.ResponseWriter, r *http.Request) error {
	//question := r.Header.Get("question")
	imageUrl, _ := s.requests.GetImageForQuestion("food")

	session := &QuizSession{
		CurrentImage: imageUrl,
	}

	return WriteJSON(w, http.StatusOK, session)
}

func (s *WebServer) handleUpdateImage(w http.ResponseWriter, r *http.Request) error {
	question, err := s.getQuestion(r)
	if err != nil {
		return err
	}

	imageUrl, err := s.requests.GetImageForQuestion(question["question"].(string))

	if err != nil {
		return err
	}

	htmlStr := fmt.Sprintf("<img src='%s' alt='ee'/>", imageUrl)

	// Write the HTML content to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, htmlStr)

	return nil
}

func (s *WebServer) handleLoadStages(w http.ResponseWriter, r *http.Request) error {
	htmlStr := templates.RenderStages()

	// Write the HTML content to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, htmlStr)
	

	return nil
}

func (s *WebServer) handleNoPlease(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (s *WebServer) handleUpdateStages(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	stage := mux.Vars(r)["stage"]
	token, _ := getToken(r)

	progress, err := s.requests.GetProgress(id, token)
	if err != nil {
		return err
	}

	progressMap, _ := progress["level"].(map[string]interface{})
	stages, _ := progressMap["progress"].(map[string]interface{})
	_, ok := stages["Unit"+stage].(map[string]interface{})
	stageNumber, _ := strconv.Atoi(stage)

	if !ok && stageNumber != 1 {
		HTML := fmt.Sprintf("Stage %s <i id='LockStage%s' class='nf nf-cod-lock'></i>", stage, stage)

		// Write the HTML content to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprintf(w, HTML)
	} else {
		HTML := fmt.Sprintf("Stage %s", stage)

		// Write the HTML content to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprintf(w, HTML)

	}

	return nil
}

func (s *WebServer) handleGetProgress(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	stages := []string{"1", "2", "3"}
	token, _ := getToken(r)
	istheLastUnlocked := ""

	progress, err := s.requests.GetProgress(id, token)
	if err != nil {
		return err
	}

	for _, stage := range stages {
		lvl, _ := progress["level"].(map[string]interface{})
		stages, _ := lvl["progress"].(map[string]interface{})
		_, ok := stages["Unit"+stage].(map[string]interface{})
		if ok {
			istheLastUnlocked = stage
		}
	}

	return WriteJSON(w, http.StatusOK, istheLastUnlocked)
}

func (s *WebServer) handleFirstTimeConfigurationRedir(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")

	http.Redirect(w, r, "/"+id+"/account_configuration", http.StatusSeeOther)

	return nil
}

func (s *WebServer) handleFirstTimeConfiguration(w http.ResponseWriter, r *http.Request) error {
	loggedIn := LoggedIn(r, w)
	if loggedIn == nil {
		tmpl, _ := template.ParseFiles(staticPages + "/configuration.html")
		tmpl.Execute(w, nil)
	}

	return nil
}

func (s *WebServer) handleUpdateAvailableLangByLang(w http.ResponseWriter, r *http.Request) error {
	var html strings.Builder
	choosenLang := r.Header.Get("lang")
	langflag, _ := s.requests.GetLanguageByName(choosenLang)
	
	html.WriteString(fmt.Sprintf(`
	<div class="language-card-container">
	<br>
	<div id="%s" class="language-card"><img style="margin-right: 10px" src="%s" width="50"/> %s</div>
	`, choosenLang, langflag["flagpath"], FirstLetterToUpper(choosenLang)))

	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, "%s", html.String())
	return nil
}

func (s *WebServer) handleUpdateAvailableLang(w http.ResponseWriter, r *http.Request) error {
	id := getCookie(r, "id")
	token, _ := getToken(r)
	languages, _ := s.requests.GetLanguages()

    settings, _ := s.requests.GetSettings(id, token)

    defaultLanguage, _ := settings["defaultLanguage"].(string)

	var html strings.Builder

	unlockedIsRun := false
	//lockedIsRun := false

	for _, lang := range languages {
		language, _ := lang["language"].(string)
		unlocked, _ := s.requests.GetProgressByLang(id, language, r)

		_, ok := unlocked["error"].(string)
		if  !ok {
			if !unlockedIsRun {
				html.WriteString(fmt.Sprintf(`
				<div class="chat-bubble-start">
				<h3>
				Your Languages
				</h3></div>`))
			}
			
			html.WriteString(fmt.Sprintf(`
			<div class="language-card-container">
			<br>
			<div id="%s" class="language-card">
			<div class="text-wrapper-top-right">
			<a id="remove%s" class='close-btn-lang close-btn'>&times;</a>
			</div>
			<div class="text-wrapper-top-left">
			<label class="custom-checkbox">
			`, language,language))
			
			if defaultLanguage == language {
				html.WriteString(fmt.Sprintf(`
				<input name="%s" id="switch-%s" onchange="defaultLanguageCheckMark(event)" class="lang-check" type="checkbox" checked>
				`, language, language))
			} else {
				html.WriteString(fmt.Sprintf(`
				<input name="%s" id="switch-%s" onchange="defaultLanguageCheckMark(event)" class="lang-check" type="checkbox">
				`,language, language))
			}

			html.WriteString(fmt.Sprintf(`
			<span class="checkmark"></span>
			</label>
			</div>
			<div class="row"><div class="col-4"><img style="margin-right: 10px" src="%s" width="50"/></div> 
			<div class="col-6"> %s </div></div></div></div>
			`, lang["flagpath"], FirstLetterToUpper(language)))

			unlockedIsRun = true
		} else {


			html.WriteString(fmt.Sprintf(`
			<hr>
			<h3>Languages available</h3>
			<br>
			<h4><strong>Please Note:</strong></h4>
			<p>Cannot add more languages. Only <strong>Azerbajani</strong> language is available at
			this time</p>
			<div id="addMoreLanguageOptions" class="language-card-container">
			<br>
			<div id="%s" class="language-card">
			<div class="text-wrapper-top-right">
			<a id="add%s" class='plus-btn'>&times;</a>
			</div>
			<div class="row"><div class="col-4"><img style="margin-right: 10px" src="%s" width="50"/></div> 
			<div class="col-6"> %s </div></div></div>
			`, language, language, lang["flagpath"], FirstLetterToUpper(language)))

			html.WriteString("</div>")
		}

	}
	

	// Write the HTML content to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(w, "%s", html.String())

	return nil

}

func (s *WebServer) handleConfigurationMenu(w http.ResponseWriter, r *http.Request) error {
		tmpl, _ := template.ParseFiles(static + "/profile_language.html")
		// Create a buffer to hold the template output
		var tmplBuffer bytes.Buffer

		// Execute the template and write its output to the buffer
		err := tmpl.Execute(&tmplBuffer, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return err
		}

		// Combine the template output with the HTML string
		combinedOutput := tmplBuffer.String()


		// Write the combined output to the response
		w.Header().Set("Content-Type", "text/html")
		fmt.Fprint(w, combinedOutput)

		return nil

}

func getCookie(r *http.Request, cookieName string) string {
	var cookieValue string
	cookies := r.Cookies()

	for _, cookie := range cookies {
		if cookie.Name == cookieName {
			cookieValue = cookie.Value
		}
	}

	return cookieValue
}

func AreYouLoggedIn(r *http.Request) bool {
	id := getCookie(r, "id")

	if id == "" {
		return false // signed out
	} else {
		return true // signed in
	}
}


func LoggedInMiddleWare(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		_, err := getToken(r)
		if err != nil {
			http.Redirect(w, r, "/login", http.StatusSeeOther)
		}

		handlerFunc(w, r)
	}
}

func LoggedIn(r *http.Request, w http.ResponseWriter) error {
	id := getCookie(r, "id")
	usedId := mux.Vars(r)["id"]

	if id == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return fmt.Errorf("failed")
	} else if id != usedId {
		http.Redirect(w, r, "/"+id+"/page_not_found", http.StatusSeeOther)
		return fmt.Errorf("failed")
	}

	return nil
}

func (s *WebServer) CheckIfUserIsNew(r *http.Request, w http.ResponseWriter, token string) error {
	id := getCookie(r, "id")

	settings, err := s.requests.GetSettings(id, token)
	if err != nil {
		return err
	}
	newAccount, _ := settings["isNewAccount"].(bool)

	if newAccount {
		return fmt.Errorf("account need to be configured")
	}

	return nil
}

func getToken(r *http.Request) (string, error) {
	session, err := store.Get(r, "session-name")
	if err != nil {
		return "", err
	}
	token, ok := session.Values["token"].(string)
	if !ok {
		return "", fmt.Errorf("access denied")
	}
	return token, nil
}

func (s *WebServer) getQuestion(r *http.Request) (map[string]interface{}, error) {
	//question := getCookie(r, "question")
	id := getCookie(r, "id")
	session, err := store.Get(r, "session-name")
	if err != nil {
		return nil, err
	}

	sessionData, err := s.requests.GetUserSessionData(id, session.Values["sessionId"].(string), session.Values["token"].(string))
	currentQuestion := int(sessionData["currentQuestion"].(float64))
	sentences := (sessionData["sentences"]).(map[string]interface{})
	question := sentences[strconv.Itoa(currentQuestion)].(map[string]interface{})

	return question, nil

}

func getUniqueMistakesFromSession(sessions []map[string]interface{}) []string {
	isUsed := make(map[string]int)
	yourMistakes := []string{}
	for _, ses := range sessions {
		sentences := ses["sentences"].(map[string]interface{})
		for key := range sentences {
			question := sentences[key].(map[string]interface{})
			success, ok := question["success"].(float64)

			if ok {
				if int(success) == 0 {

					if isUsed[question["question"].(string)] != 1 {
						yourMistakes = append(yourMistakes, question["question"].(string))
					}

					isUsed[question["question"].(string)] = 1
				}
			}

		}
	}

	return yourMistakes
}

func (s *WebServer) GetUserInformation(id string, r *http.Request, w http.ResponseWriter) *UserData {
	profileData, err := s.requests.GetUserProfileByID(id, r)

	if err != nil {
		http.Redirect(w, r, "/account/sign_out", http.StatusSeeOther)
	}

	userData := &UserData{}

	userData.Email, _ = profileData["email"].(string)
	userData.UserName, _ = profileData["username"].(string)
	userData.FirstName, _ = profileData["firstname"].(string)
	userData.LastName, _ = profileData["lastname"].(string)
	userData.FullName = userData.FirstName + " " + userData.LastName

	return userData
}

func (s *WebServer) GetUserProgress(id string, r *http.Request, w http.ResponseWriter) *ProgressData {
	progress, err := s.requests.GetAllProgress(id, r)

	if err != nil {
		http.Redirect(w, r, "/account/sign_out", http.StatusSeeOther)
	}

	progressData := &ProgressData{
		Data: make(map[string]*Progress),
	}

	for index := range progress {
		level, _ := progress[index]["level"].(map[string]interface{})
		progressSubData := &Progress{}
		language := progress[index]["language"].(string)
		language = FirstLetterToUpper(language)

		progressSubData.Language = language
		progressSubData.Level = level

		progressData.Data[language] = progressSubData
	}

	return progressData
}

func (s *WebServer) GetUserDefaultLang(id string, r *http.Request, w http.ResponseWriter) string {
	token, err := getToken(r)
	if err != nil {
		return ""
	}
	settings, _ := s.requests.GetSettings(id, token)

	language, _ := settings["defaultLanguage"].(string)
	if language == "" {
		language = "azerbajani"
	}

	language = strings.TrimSpace(language)

	return language
}

func showProfileSection(
	r *http.Request, w http.ResponseWriter,
	html, section string, profile profileSectionData) error {

	session, _ := store.Get(r, "session-name")
	session.Values["section"] = section
	session.Save(r, w)

	tmpl, err := template.New("profileSection").Parse(html)
	if err != nil {
		http.Error(w, "Unable to parse template", http.StatusInternalServerError)
		return nil
	}
	if err := tmpl.Execute(w, profile); err != nil {
		http.Error(w, "Unable to execute template", http.StatusInternalServerError)
	}

	return nil

}

func FirstLetterToUpper(word string) string {
	leftOver := word[1:]
	word = strings.ToUpper(word[:1])
	word = word + leftOver

	return word
}

func listFiles(dir string) ([]string, error) {
	files, err := os.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	filesInDir := []string{}
	for _, file := range files {
		if !file.IsDir() {
			filesInDir = append(filesInDir, file.Name())
		}
	}

	return filesInDir, nil
}

func CheckIfDirExist(dir string) (bool, error){
	info, err := os.Stat(dir)
	if os.IsNotExist(err) {
		return false, err
	} else if err != nil {
		fmt.Println("Error:", err)
		return false, err
	} else if info.IsDir() {
		return true, nil
	} else {
		return false, nil
	}
}

func UploadImage(r *http.Request, w http.ResponseWriter, pictureLocation string) (string, error) {
	if r.Method != "POST" {
		http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
		return "", nil
	}

	// Parse the multipart form
	err := r.ParseMultipartForm(10 << 20) // 10 MB limit
	if err != nil {
		http.Error(w, "Error parsing form data", http.StatusBadRequest)
		return "", err
	}

	// Get the file from the request
	file, header, err := r.FormFile("image")
	if err != nil {
		http.Error(w, "Error retrieving file from form data", http.StatusBadRequest)
		return "", err
	}
	defer file.Close()

	// Create directories if they don't exist
	uploadDir := "./user_data"
	pictureDir := filepath.Join(uploadDir, pictureLocation)
	if err := os.MkdirAll(pictureDir, 0755); err != nil {
		http.Error(w, "Error creating directories", http.StatusInternalServerError)
		return "", err
	}

	// Create the file path
	filePath := filepath.Join(pictureDir, header.Filename)
	filePath = strings.ReplaceAll(filePath, " ", "_")
	outFile, err := os.Create(filePath)
	if err != nil {
		http.Error(w, "Error creating file", http.StatusInternalServerError)
		return "", err
	}
	defer outFile.Close()

	// Copy the uploaded file to the created file
	_, err = io.Copy(outFile, file)
	if err != nil {
		http.Error(w, "Error saving file", http.StatusInternalServerError)
		return "", err
	}

	// Send success message
	message := templates.RenderMessageStr(fmt.Sprintf("File uploaded successfully: %s", header.Filename))
	fmt.Fprintf(w, message)
	return filePath, nil

}

func displayTemplateWithHeaderFooter(w http.ResponseWriter, file string, data any) error{
	tmpl, _ := template.ParseFiles(file)
	// Create a buffer to hold the template output
	var tmplBuffer bytes.Buffer

	// Execute the template and write its output to the buffer
	err := tmpl.Execute(&tmplBuffer, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return err
	}

	footer, _ := templates.RenderFooter()
	header, _ := templates.RenderHeader()

	// Combine the template output with the HTML string
	combinedOutput := header + tmplBuffer.String() + footer

	// Write the combined output to the response
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, combinedOutput)
	return nil
}

type webFunc func(http.ResponseWriter, *http.Request) error

type webError struct {
	Error string `json:"error"`
}

func makeHTTPHandleFunc(f webFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := f(w, r); err != nil {
			WriteJSON(w, http.StatusBadRequest, webError{Error: err.Error()})
		}
	}
}

func WriteJSON(w http.ResponseWriter, status int, v any) error {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(v)

}

// hxredirect sets the HX-Redirect header to the specified URL
func hxredirect(w http.ResponseWriter, url string) {
	w.Header().Set("HX-Redirect", url)
	w.WriteHeader(http.StatusOK)
}

