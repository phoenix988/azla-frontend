package main

import (
	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
	"net/http"
)

func CreateRoutes(s *WebServer) (*mux.Router, func(http.Handler) http.Handler) {
	router := mux.NewRouter()
	
	// Account related handlers
	router.HandleFunc("/", makeHTTPHandleFunc(s.handleRedirect))
	router.HandleFunc("/login", makeHTTPHandleFunc(s.handleLoginMenu))
	router.HandleFunc("/logout", makeHTTPHandleFunc(s.handleLogout))
	router.HandleFunc("/create_account", makeHTTPHandleFunc(s.handleCreateAccountMenu))
	router.HandleFunc("/account/sign_out", makeHTTPHandleFunc(s.handleSignOut))
	router.HandleFunc("/account/sign_in", makeHTTPHandleFunc(s.handleSignIn))
	router.HandleFunc("/login/redirect", makeHTTPHandleFunc(s.handleLoginRedirect))

	// account first time config
	router.HandleFunc("/{id}/account_configuration", makeHTTPHandleFunc(s.handleFirstTimeConfiguration))
	router.HandleFunc("/account/configuration", makeHTTPHandleFunc(s.handleFirstTimeConfigurationRedir))
	
	// validate routes
	router.HandleFunc("/validate/login/{method}", makeHTTPHandleFunc(s.handleValidate))
	router.HandleFunc("/validate/sure", makeHTTPHandleFunc(s.handleValidateExit))

	// dialog
	router.HandleFunc("/dialog/progress/yes", makeHTTPHandleFunc(s.handleYesDeleteProgress))
	router.HandleFunc("/dialog/quiz/no", makeHTTPHandleFunc(s.handleNoPlease))
	router.HandleFunc("/dialog/no", makeHTTPHandleFunc(s.handleNoPlease))
	
	// no data found
	router.HandleFunc("/{id}/page_not_found", makeHTTPHandleFunc(s.handleNoData))

	// menus 
	router.HandleFunc("/{id}", makeHTTPHandleFunc(s.handleMenuId))
	router.HandleFunc("/menu", makeHTTPHandleFunc(s.handleMenuNoId))
	router.HandleFunc("/{id}/menu", LoggedInMiddleWare(makeHTTPHandleFunc(s.handleMenu)))
	router.HandleFunc("/{id}/menu/{aze}", makeHTTPHandleFunc(s.handleStageMenu))
	router.HandleFunc("/{id}/menu/{aze}/{stage}", LoggedInMiddleWare(makeHTTPHandleFunc(s.handleStageMenu)))
	router.HandleFunc("/{id}/menu/{aze}/review/mistakes", makeHTTPHandleFunc(s.handleReviewMistakes))
	router.HandleFunc("/get/menu/{stage}", makeHTTPHandleFunc(s.handleStageMenuRedirect))
	router.HandleFunc("/redirect/stage", makeHTTPHandleFunc(s.handleExitQuiz))

	// progress
	router.HandleFunc("/{id}/progress/{aze}", makeHTTPHandleFunc(s.handleProgressMenu))
	router.HandleFunc("/get/progress", makeHTTPHandleFunc(s.handleProgressRedirect))
	router.HandleFunc("/get/current/progress", makeHTTPHandleFunc(s.handleGetProgress))
	
	// profile
	router.HandleFunc("/{id}/profile", makeHTTPHandleFunc(s.handleProfile))
	router.HandleFunc("/1/profile", makeHTTPHandleFunc(s.handleProfileRedirect))
	router.HandleFunc("/profile", makeHTTPHandleFunc(s.handleProfileRedirect))
	router.HandleFunc("/go/profile/{section}", makeHTTPHandleFunc(s.handleGoToProfileSection))
	
	// quiz handlers
	router.HandleFunc("/{id}/quiz/{session}/{cat}/{lvl}", makeHTTPHandleFunc(s.handleRunQuiz))
	router.HandleFunc("/{id}/quiz/{session}", makeHTTPHandleFunc(s.handleRunQuiz))
	router.HandleFunc("/{id}/quiz/{session}/end", makeHTTPHandleFunc(s.handleLastQuestion))
	router.HandleFunc("/{id}/quiz/{session}/no_health", makeHTTPHandleFunc(s.handleNoHealth))
	router.HandleFunc("/quiz/start", makeHTTPHandleFunc(s.handleStartQuiz))
	router.HandleFunc("/quiz/main/start", makeHTTPHandleFunc(s.handleStartMainQuiz))

	// quiz review session
	router.HandleFunc("/quiz/review/start", makeHTTPHandleFunc(s.handleStartReview))
	router.HandleFunc("/quiz/review/redirect", makeHTTPHandleFunc(s.handleStartReviewRedir))
	router.HandleFunc("/{id}/no_health", makeHTTPHandleFunc(s.handleNoHealth))
	
	// get image for current question
	router.HandleFunc("/get/image", makeHTTPHandleFunc(s.handleGetImage))
	router.HandleFunc("/get/review/mistake", makeHTTPHandleFunc(s.handleReviewRedirect))
	
	// update handlers
	router.HandleFunc("/update/image", makeHTTPHandleFunc(s.handleUpdateImage))
	router.HandleFunc("/update/mistakes", makeHTTPHandleFunc(s.handleUpdateMistakes))
	router.HandleFunc("/update/progress_menu", makeHTTPHandleFunc(s.handleProgressMenu))
	router.HandleFunc("/update/configuration_menu", makeHTTPHandleFunc(s.handleConfigurationMenu))
	router.HandleFunc("/update/stage/{stage}", makeHTTPHandleFunc(s.handleUpdateStages))
	router.HandleFunc("/update/profile/{section}", makeHTTPHandleFunc(s.handleProfileSections))
	router.HandleFunc("/update/{action}", makeHTTPHandleFunc(s.handleUpdates))
	
	// token handler
	router.HandleFunc("/get/token", makeHTTPHandleFunc(s.handleGetToken))
	router.HandleFunc("/get/last_menu", makeHTTPHandleFunc(s.handleLastStageMenu))
	
	// upload routes
	router.HandleFunc("/upload/image", makeHTTPHandleFunc(s.handleUploadImage))
	router.HandleFunc("/upload/image_temp", makeHTTPHandleFunc(s.handleUploadImageTemp))
	
	router.HandleFunc("/list/images", makeHTTPHandleFunc(s.handleListImages))

	// Serve files from the "/static/ and /assets/" directory
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))
	router.PathPrefix("/static/pages").Handler(http.StripPrefix("/static/pages", http.FileServer(http.Dir("./static/pages"))))
	router.PathPrefix("/assets/css").Handler(http.StripPrefix("/assets/css", http.FileServer(http.Dir("./assets/css"))))
	router.PathPrefix("/assets/js").Handler(http.StripPrefix("/assets/js", http.FileServer(http.Dir("./assets/js"))))
	router.PathPrefix("/images").Handler(http.StripPrefix("/images", http.FileServer(http.Dir("./images"))))
	router.PathPrefix("/css/").Handler(http.StripPrefix("/css/", http.FileServer(http.Dir("./css/"))))
	router.PathPrefix("/user_data").Handler(http.StripPrefix("/user_data", http.FileServer(http.Dir("./user_data"))))

	router.PathPrefix("/vend/").Handler(http.StripPrefix("/vend/", http.FileServer(http.Dir("./vend/"))))
	router.PathPrefix("/vend/select2").Handler(http.StripPrefix("/vend/select2", http.FileServer(http.Dir("./vend/select2"))))
	router.PathPrefix("/fonts").Handler(http.StripPrefix("/fonts/", http.FileServer(http.Dir("./fonts/"))))
	router.PathPrefix("/assets/fonts").Handler(http.StripPrefix("/assets/fonts/", http.FileServer(http.Dir("./assets/fonts/"))))

	// Serve files from the "/theme/" directory
	router.PathPrefix("/theme/").Handler(http.StripPrefix("/theme/", http.FileServer(http.Dir("./theme/"))))
	router.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets/"))))

	// Serve files from the "/src/" directory
	router.PathPrefix("/src/").Handler(http.StripPrefix("/src/", http.FileServer(http.Dir("./src/"))))
	router.HandleFunc("/static/styles.css", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/css")
		http.ServeFile(w, r, "static/styles.css")
	})

	router.HandleFunc("/assets/css/styles.css", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/css")
		http.ServeFile(w, r, "assets/styles.css")
	})
	
	router.HandleFunc("/assets/css/dark-mode.css", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/css")
		http.ServeFile(w, r, "assets/css/dark-mode.css")
	})

	corsHandler := handlers.CORS(
		handlers.AllowedHeaders([]string{"Content-Type", "Authorization"}),
		handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"}),
		handlers.AllowedOrigins([]string{"*"}),
	)


	return router, corsHandler
}
