var rootStyles = getComputedStyle(document.documentElement);
var normalBlueColor = rootStyles.getPropertyValue("--normal-blue").trim();
var mainAccentColor = rootStyles.getPropertyValue("--accent-color-main").trim();
var whiteBackground = rootStyles.getPropertyValue("--main-background").trim();
var selectedLanguage


function createProgressForLanguage(language) {
    var id = getUserIdFromUrl()
    // Create a data object with the body of your POST request
    var data = {};

    var requestOptions = createRequestOptions("POST", data)

    sendFetchUrl(`${apiUrl}/progress/api/${id}/${language}`, requestOptions).then((data) => {
    });
}

function defaultLanguageCheckMark(event) {
    var langCheck = document.querySelectorAll(".lang-check")

    langCheck.forEach((lang) => {
        lang.checked = false
    });

    event.target.checked = true
    updateSettingForAccount("language", event.target.name)

}


function createRemoveLanguageButton() {
    const closeBtn = document.querySelectorAll(".close-btn-lang");

    if (closeBtn) {
        closeBtn.forEach((btn) =>{
            btn.addEventListener("click", function() {
                var lang = this.id.replace("remove", "");
                var languageCard = document.getElementById(lang);
                var parent = languageCard.parentNode;
                console.log(parent)

                // Function to handle the transition end and remove the card
                function handleTransitionEnd() {
                    languageCard.remove();
                }

                // Add the slide-out class to trigger the animation
                languageCard.classList.add("slide-out");

                // Create and style the Undo button
                if (!document.getElementById(`confirmButton${lang}`)) {
                    var confirmButton = document.createElement("a");
                    confirmButton.id = `confirmButton${lang}`
                    confirmButton.textContent = "Confirm"; confirmButton.classList.add("undo-btn")
                    confirmButton.addEventListener("click", function() {
                        var unlockQuestions = document.getElementById("unlockLanguages")
                        var requestOptions = createRequestOptions("DELETE") 
                        var id = getCookie("id")
                        sendFetchUrl(`${apiUrl}/progress/api/${id}/${lang}`, requestOptions).then(() => {
                            fetch('/update/choosen_language', {
                                method: 'GET',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'lang': `${lang}`,
                                }
                            })
                                .then(response => response.text()) // Assuming the server returns plain text or HTML
                                .then(data => {
                                    document.getElementById("buttonContainer").style.display = "block";
                                    unlockQuestions.innerHTML = data

                                    confirmButton.remove()
                                    button.remove()

                                    selectCard()

                                })
                                .catch(error => console.error('Error:', error));
                        });
                    });
                } 

                // Add the transitionend event listener
                languageCard.addEventListener("transitionend", handleTransitionEnd);


                if (!document.getElementById(`undoBtn${this.id}`)) {
                    var button = document.createElement("a");
                    button.textContent = "Undo";
                    button.classList.add("undo-btn");
                    button.id = "undoBtn"+this.id

                    button.addEventListener("click", function() {
                        confirmButton.remove()
                        // Remove the slide-out class to restore the card
                        languageCard.classList.remove("slide-out");

                        // Insert the languageCard back into the DOM before the Undo button
                        parent.insertBefore(languageCard, button);

                        // Remove the Undo button from the DOM
                        parent.removeChild(button);

                        // Remove the transitionend event listener to prevent the card from being removed again
                        languageCard.removeEventListener("transitionend", handleTransitionEnd);

                    });


                    parent.appendChild(button);
                    parent.appendChild(confirmButton)

                } else {
                    var button = document.getElementById(`undoBtn${this.id}`)

                    button.addEventListener("click", function() {
                        // Remove the slide-out class to restore the card
                        languageCard.classList.remove("slide-out");

                        // Remove the transitionend event listener to prevent the card from being removed again
                        languageCard.removeEventListener("transitionend", handleTransitionEnd);

                    });

                }

            });
        });

    }

}

function selectCard() {
    const textColor = rootStyles.getPropertyValue("--text-color").trim();
    var languageCards = document.querySelectorAll(".language-card")
    saveTokenToStorage()
    languageCards.forEach((card) => {
        card.addEventListener("click", function() {
            clearSelection()
            this.style.backgroundColor = mainAccentColor;
            this.style.color = textColor;
            selectedLanguage = this.textContent
        });
    });

    function clearSelection() {
        languageCards.forEach((card) => {
            card.style.backgroundColor = "";
            card.style.color = "";
        });
    }
}


function confirmLanguageAction() {
    createProgressForLanguage(selectedLanguage.trim())
    updateSettingForAccount("language", selectedLanguage.trim())

    document.getElementById('languageSelection').style.display = "none";
    document.getElementById('selectStage').style.display = "block";
    document.getElementById('langMessage').style.display = "block";
    document.getElementById('langMessage').textContent = `${selectedLanguage} Unlocked`;

    fadeIn(document.getElementById('selectStage'))
}

document.addEventListener("htmx:afterSettle", function() {
    selectCard()
    var submitButton = document.getElementById("submitButton")
    if (submitButton) {
        submitButton.addEventListener("click", function() {
            if (!selectedLanguage) {
                return console.log("nothing selected")
            }

            updateSettingForAccount("language", selectedLanguage.trim())
            updateSettingForAccount("new_account", "false")

            createProgressForLanguage(selectedLanguage.trim())
            document.getElementById(selectedLanguage.trim().toLowerCase()).innerHTML = "";

            var message = `progress for ${selectedLanguage.trim()} created`
            var div 
            var content = document.getElementById("messageContent")
            if (!content) {
                div = document.createElement("div"); div.id = "messageContent"
                div.textContent = message
                document.getElementById("configMessage").appendChild(div)
            } else {
                div = document.getElementById("messageContent")
                div.textContent = message
            }

            document.getElementById("configMessage").style.display = "block"
            document.getElementById("closeButton").addEventListener("click", function() {
                document.getElementById("configMessage").style.display = "none"
            });

            fetch('/update/configuration_menu', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'lang': `${selectedLanguage.trim().toLowerCase()}`,
                }
            })
                .then(response => response.text()) // Assuming the server returns plain text or HTML
                .then(data => {
                    document.getElementById("settingsForm").innerHTML = data;
                    window.location.reload()

                })
                .catch(error => console.error('Error:', error));
        });

    }
    
    function activateDarkModeSwitch() {
        const themeSwitch = document.getElementById("darkModeSwitch");
        if (themeSwitch) {
            themeSwitch.addEventListener("change", function(event) {
                updateSettingForAccount("dark_mode", themeSwitch.checked)
            });

            fetchSettings().then((settings) => {
                if (settings.darkmode) {
                    themeSwitch.checked = true;
                } else {
                    themeSwitch.checked = false;

                }
            });
        }
    }


    function getSettings() {
        const title = document.getElementById("title")
        if (title) {
            fetchSettings().then((data) => {
                if (data.isNewAccount) {
                    //title.textContent = "First Time Configuration";
                } else {
                    //title.textContent = "Account Configuration";
                }
            });
        }
    }

    document.addEventListener("DOMContentLoaded", activateDarkModeSwitch())
    document.addEventListener("DOMContentLoaded", getSettings())
    document.addEventListener("DOMContentLoaded", createRemoveLanguageButton())
    
    window.onclick = function(event) {
        if (!event.target.matches(".language-card"))  {
            var languageCard = document.getElementsByClassName("language-card");
            var i;
            for (i = 0; i < languageCard.length; i++) {
                var language = languageCard[i];
                    language.style.backgroundColor = "";
            }

        }
    };
});
