var validate = {
    "password": function() {
        validatePassword()
    },

    "email": function() {
        validateEmail()
    },

    "name": function() {
        validateName()
    },

    "username": function() {
        validateUsername()
    }
}

// Request to create the user
function checkIfSuccess(data) {
    // Handle the response data
    var checkOk = true;

    if (data.emailFail) {
        document.getElementById("email").classList.add("invalid-input");
        checkOk = false;
    }

    if (data.usernameFail) {
        document.getElementById("username").classList.add("invalid-input");
        checkOk = false;
    }

    if (data.nameFail) {
        document.getElementById("firstname").classList.add("invalid-input");
        document.getElementById("lastname").classList.add("invalid-input");
        checkOk = false;
    }

    if (data.passwordFail) {
        document.getElementById("password").classList.add("invalid-input");
        checkOk = false;
    }

}

function createUser(event) {
    var loginData = {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value,
        passwordConfirm: document.getElementById("passwordconfirm").value,
        email: document.getElementById("email").value,
        lastname: document.getElementById("lastname").value,
        firstname: document.getElementById("firstname").value,
    }
    event.preventDefault();
    
    const inputIds = [
        "username",
        "password",
        "passwordconfirm",
        "email",
        "lastname",
        "firstname",
    ];

    inputIds.forEach((id) => {
        const inputElement = document.getElementById(id);

        // Remove the invalid class
        inputElement.classList.remove("invalid-input");
    });

    // Create a data object with the body of your POST request
    var data = {
        firstName: loginData.firstname,
        lastName: loginData.lastname,
        email: loginData.email,
        password: loginData.password,
        confirmPassword: loginData.passwordConfirm,
        username: loginData.username,
    };

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Specify the content type
        },
        body: JSON.stringify(data),
    };

    // Make the POST request
    sendFetchUrl(`${apiUrl}/account`, requestOptions).then((data) => {
        checkIfSuccess(data)

        if (data.username) {
            window.location.href = "/login";
        }
    });
}

document.addEventListener("DOMContentLoaded", function() {
    // Checks if you enter valid email address
    validate["email"]()

    // check password
    validate["password"]()

    validate["name"]()

    validate["username"]()
});


// validate on submit
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    // Validate on blur event
    input.each(function(){
        $(this).on('blur', function(){
            validate(this);
        });
    });

    function validate(input) {
        if($(input).attr('type') === 'email' || $(input).attr('name') === 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) === null) {
                showValidate(input);
            } else {
                hideValidate(input);
            }
        }
        else {
            if($(input).val().trim() === ''){
                showValidate(input);
            } else {
                hideValidate(input);
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }

})(jQuery);

(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        createUser(event)

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);
