function validateEmail(currentEmail) {
    const emailInput = document.getElementById("email");

    if (emailInput) {
        emailInput.addEventListener("blur", function() {
            emailInput.classList.remove("invalid-input");
            const email = emailInput.value;
            const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

            if (emailInput.value !== currentEmail) {
                if (!emailRegex.test(email)) {
                    emailInput.classList.add("invalid-input");
                } else {
                    emailInput.classList.remove("invalid-input");
                    fetch(`${apiUrl}/email/check/${emailInput.value}`)
                        .then((response) => {
                            if (!response.ok) {
                                throw new Error("Network response was not ok");
                            }
                            return response.json(); // Parse the JSON response
                        })
                        .then((data) => {
                            if (data) {
                                emailInput.classList.add("invalid-input");
                            } else {
                                emailInput.classList.remove("invalid-input");
                            }
                        });
                }
            }
        });
    }
}

function validatePassword() {
    const password = document.getElementById("password");
    const passwordConfirm = document.getElementById("passwordconfirm");

    if (password && passwordConfirm) {
        password.addEventListener("blur", function() {
            if (password.value !== passwordConfirm.value) {
                password.classList.add("invalid-input");
                passwordConfirm.classList.add("invalid-input");
            } else {
                password.classList.remove("invalid-input");
                passwordConfirm.classList.remove("invalid-input");
            }
        });

        passwordConfirm.addEventListener("blur", function() {
            if (password.value !== passwordConfirm.value) {
                password.classList.add("invalid-input");
                passwordConfirm.classList.add("invalid-input");
            } else {
                password.classList.remove("invalid-input");
                passwordConfirm.classList.remove("invalid-input");
            }
        });
    }
}

function validateUsername(currentUsername) {
    const username = document.getElementById("username");
    if (username) {
        if (username.value !== currentUsername) {
            username.addEventListener("blur", function() {
                if (!username.value) {
                    username.value = "";
                } else {
                    fetch(`${apiUrl}/user/${username.value}`)
                        .then((response) => {
                            if (!response.ok) {
                                throw new Error("Network response was not ok");
                            }
                            return response.json(); // Parse the JSON response
                        })
                        .then((data) => {
                            if (data) {
                                username.classList.add("invalid-input");
                            } else {
                                username.classList.remove("invalid-input");
                            }
                        })
                        .catch((error) => {
                            console.error("There was a problem with the request:", error);
                        });
                }
            });
        }

    }

}

function validateName() {
    const lastname = document.getElementById("lastname");
    const firstname = document.getElementById("firstname");

    firstname.addEventListener("blur", function() {
        if (firstname.value == "") {
            firstname.classList.add("invalid-input");
        } else {
            firstname.classList.remove("invalid-input");
        }
    });

    lastname.addEventListener("blur", function() {
        if (lastname.value == "") {
            lastname.classList.add("invalid-input");
        } else {
            lastname.classList.remove("invalid-input");
        }
    });

}
