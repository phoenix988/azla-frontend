function validateLogin(event) {
    event.preventDefault();
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    // Create a data object with the body of your POST request
    var data = {
        userName: username,
        password: password,
    };

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Specify the content type
        },
        body: JSON.stringify(data),
    };

    let darkMode = getCookie("darkmode");

    if (darkMode) {
        if (darkModeSwitch) {
            darkModeSwitch.checked = true;
        }

        toggleDarkModeMenu();
    }

    loginRequest(requestOptions, username, password);
}

function loginRequest(requestOptions, username, password) {
    // Perform validation or send AJAX request to the server to validate credentials
    // For demonstration purpose, assume validation fails
    var isValid = false; // Set this to true if validation succeeds
    var profileId 

    hideErrorMessage()
    
    // Make the POST request
    fetch(`${apiUrl}/login`, requestOptions)
        .then((response) => {
            switch(response.status) {
            case 406:
                console.log("server error")
            case 403:
                console.log("login failed")
            }
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }

            return response.json(); // Parse the JSON response
        })
        .then((data) => {
            // Handle the response data
            isValid = data;
            if (isValid) {
                saveUserInfo(data, username, password)
                const test = getToken()
                getUserProfile(username)
                    .then((profile) => {
                        profileId = profile.id
                        if (profile.id) {
                            configureUserSession(profile, data)
                        }
                    })
                    .catch((error) => {
                        console.error("Error:", error);
                    }).finally(() => {;
                        if (!profileId) {
                            getUserProfileByUserName(username).then((profile) => {
                                configureUserSession(profile)
                            }).error((error) => {
                                console.log(error)
                            });
                        }
                    });
            }
        })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
        })
        .finally(() => {
            if (!isValid) {
                // Add invalid-input class to the input fields
                document
                    .getElementById("username")
                    .classList.add("invalid-input");
                document
                    .getElementById("password")
                    .classList.add("invalid-input");
                document
                    .getElementById("validateLogin")
                    .classList.add("alert-validate-login");
                document
                    .getElementById("validateLoginPass")
                    .classList.add("alert-validate-login");
            }
        });
}

function configureUserSession(profile, data) {
    setCookie("id", profile.id, 365);
    createUserData(profile.id);

    fetch("/login/redirect", {
        method: 'GET',
        headers: {
            'Authorization': `${data.token}`
        }
    }).then(response => {
        if (response.redirected) {
            window.location.href = response.url;
        } else {
            // Handle other responses if necessary
            console.log('Response not redirected', response);
        }
    }).catch(error => {
        console.error('Error during fetch:', error);
    });

}

function saveUserInfo(data, username, password) {
    saveToken(data.token)
    setCookie("username", username, 365);
    showLoadingScreen()
}

function createUserData(id) {
    //createProgressOnFirstLogin(id)
    createSettingsOnFirstLogin(id)
}

function listenForInputEvents() {
    const emailInput = document.getElementById("username");
    const emailMessage = document.getElementById("validateLogin");
    const passwordMessage = document.getElementById("validateLoginPass");
    const password = document.getElementById("password");

    emailInput.addEventListener("blur", function() {
        //const email = emailInput.value;
        //const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

        emailInput.classList.remove("invalid-input");
        emailMessage.classList.remove("alert-validate-login");
    });

    emailInput.addEventListener("mouseenter", function() {
        // Execute your code when mouse enters the input box
        emailMessage.classList.remove("alert-validate-login");
    });

    password.addEventListener("blur", function() {
        password.classList.remove("invalid-input");
        passwordMessage.classList.remove("alert-validate-login");
    });

    passwordMessage.addEventListener("mouseenter", function() {
        // Execute your code when mouse enters the input box
        passwordMessage.classList.remove("alert-validate-login");
    });

}


async function createProgressOnFirstLogin(id) {
    // Create a data object with the body of your POST request
    var data = {};
    const token = getToken()

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
        body: JSON.stringify(data),
    };

    // Make the POST request
    return fetch(`${apiUrl}/progress/api/${id}`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then((data) => {return data})
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
        })
        .finally(() => { });
}

function createSettingsOnFirstLogin(id) {
    // Create a data object with the body of your POST request
    var data = {};
    const token = getToken()

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
        body: JSON.stringify(data),
    };

    // Make the POST request
    fetch(`${apiUrl}/account/${id}/settings`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then(() => { })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
        })
        .finally(() => { });
}

function hideErrorMessage() {
    document.getElementById("username").classList.remove("invalid-input");
    document.getElementById("password").classList.remove("invalid-input");
    document
        .getElementById("validateLogin")
        .classList.remove("alert-validate-login");
    document
        .getElementById("validateLoginPass")
        .classList.remove("alert-validate-login");
}


// Checks if you enter valid email address
document.addEventListener("DOMContentLoaded", function() {
    listenForInputEvents()
});
