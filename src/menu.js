var rootStyles = getComputedStyle(document.documentElement);
var normalBlueColor = rootStyles.getPropertyValue("--normal-blue").trim();
var mainAccentColor = rootStyles.getPropertyValue("--accent-color-main").trim();
var whiteBackground = rootStyles.getPropertyValue("--main-background").trim();
var checkForUnitChange = false;
var checkForStartLabel = false;

let currentIndex = 0;

// click action to open the stage menu
function addStageClickAction() {
    const section = document.querySelectorAll(".section")

    section.forEach((sect) => {
        sect.addEventListener("click", function() {

            var locked = document.getElementById("Lock"+this.id)

            if (!locked) {
                fetchSettings().then((settings) => {
                    if (settings.defaultLanguage == "") {
                        settings.defaultLanguage = "azerbajani"
                    }

                    loadProgress(settings.defaultLanguage).then((request) => {
                        if (request.status == 400) {
                            createProgressForLanguage(settings.defaultLanguage)

                        }
                        window.location.href = `menu/${settings.defaultLanguage}/${this.id}`

                    });

                });

            }
        });

    });
}

async function loadProgress(language) {
    const id = getUserIdFromUrl();
    const token = getToken()

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
    };
    
    return sendFetchUrl(`${apiUrl}/progress/api/${id}/${language}`, requestOptions).then((requestData) => {
        return requestData
    }).catch((error) => {
        return error
    });
}

async function loadProgressAll() {
    const id = getUserIdFromUrl();
    const token = getToken()

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
    };

    // Make the POST request
    return sendFetchUrl(`${apiUrl}/progress/api/${id}/get/all`, requestOptions).then((requestData) => {
        return requestData
    });
}


function loadDefaultLanguage() {
    fetchSettings().then((settings) => {
        //console.log(settings.defaultLanguage)
    });
}

function insertBefore(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode);
}

function toggleDropDown() {
    document.getElementById("dropMenu").classList.toggle("show");
}


document.addEventListener("htmx:afterSettle", function(event) {
    loadProfilePicture()
});

document.addEventListener("DOMContentLoaded", function() {
    addStageClickAction()

    loadDefaultLanguage()
    
    saveTokenToStorage()

    var profileDropDown = document.getElementById("profileDropdown")
    if (profileDropDown) {
        profileDropDown.addEventListener("click", function() {
            toggleDropDown()
        });
    }

});


