var rootStyles = getComputedStyle(document.documentElement);
var normalBlueColor = rootStyles.getPropertyValue("--normal-blue").trim();
var mainAccentColor = rootStyles.getPropertyValue("--accent-color-main").trim();
var whiteBackground = rootStyles.getPropertyValue("--main-background").trim();
var red = rootStyles.getPropertyValue("--red").trim();
var green = rootStyles.getPropertyValue("--green").trim();

function loadGameMode(mode) {
    if (mode == "drag_drop") {
        showDragDropMode();
        hideWriteMode();
        setCookie("mode", mode, 1);
    } else if (mode == "write") {
        hideDragDropMode();
        showWriteMode();
        setCookie("mode", mode, 1);
    } else {
        showDragDropMode();
        hideWriteMode();
        setCookie("mode", mode, 1);
    }

}

function loadWords() {
    let gotError = false
    let lastPath = getLastPathSegment();

    if (lastPath === "no_health") {
        return true
    }

    if (lastPath === "end") {
        return true
    }
    
    let last = getCookie("last");

    if (last) {
        lastQuestion();
    }

    var question = document.getElementById("question");
    if (question) {
        question.style.color = "0";
    }

    var pathname = window.location.pathname;
    var pathSegments = pathname.split("/");

    // Get the first path segment after the domain
    var id = pathSegments[1];
    const token = getToken();

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            Authorization: `${token}`,
        },
    };

    var sessionId = getCurrentActiveSession()
    showLoadingScreen()
    // Make the GET request
    getCurrentQuestion().then((current) => {
        fetch(`${apiUrl}/words/api/${id}/aze/${current}/ses/${sessionId}`, requestOptions)
            .then((response) => {
                if (!response.ok) {
                    throw new Error("Network response was not ok");
                }
                return response.json(); // Parse the JSON response
            })
            .then((data) => {
                retrieveSessionData().then((session) => {
                    if (session.iscompleted) {
                        console.log("session is invalid or done")
                    } else {
                        updateProgress(`${current / session.maxAmountWords * 100}`)
                        loadGameMode(data.gameMode)
                        setCookie("word", data.Question, 1)
                        // Handle the response data
                        populateQuizWithQuestion(data);
                        getHealth().then((health) => {
                            populateHealth(health);
                        });

                        let question = document.getElementById("question");
                        getCurrentQuestion().then((current) => {
                            question.textContent = `${current} ${data.question}`;
                        });
                    }
                })
                    .catch((error) => {
                        // Handle errors
                        console.error("There was a problem with the request:", error);
                        gotError = true

                    })
                    .finally(() => {
                        if (gotError) {
                            console.log("Problem occured with the quiz")
                            setTimeout(() => {
                                hideLoadingScreen()
                                loadWords()
                            }, 2000); // 0.3s (same as transition duration)
                            showLoadingScreen()
                        }
                    });
            });

    });

    const checkButton = document.getElementById("checkButton");
    const screenWidth = window.innerWidth;
    if (screenWidth < 768) {
        checkButton.style.width = "100%";
    }
}

function updateSessionCompleteStatus() {
    // Get the first path segment after the domain
    var id = getUserIdFromUrl();
    const token = getToken();
    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            Authorization: `${token}`,
        },
    }
    const sessionId = getCurrentActiveSession()
    // Make the GET request
    fetch(`${apiUrl}/account/${id}/sessions/update/${sessionId}`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then(() => { })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
        })
        .finally(() => { });

}


async function getSessionData(sessionId) {
    const id = getUserIdFromUrl();
    const token = getToken();

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            Authorization: `${token}`,
        },
    };

    // Make the POST request
    return fetch(`${apiUrl}/account/${id}/sessions/${sessionId}`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then((data) => {
            // Handle the response data
            return data;
        })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
            throw error;
        })
        .finally(() => { });

}

async function evaluateAnswerRequest(answer) {
        // remove spaces
        const token = getToken();
        const id = getUserIdFromUrl()
        const mode = getCookie("mode")
        const sessionId = getCurrentActiveSession()
        var currentQuestion = document.getElementById("question").textContent.split(" ")[0]
        if (mode == "drag_drop") {
             answer = document.getElementById("dropTarget").textContent.trim()
        }

        // Create a data object with the body of your POST request
        var data = {
            answer: answer,
        };
        
            // Create the request options, including the method, headers, and body
            var requestOptions = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json", // Specify the content type
                    Authorization: `${token}`,
                },
                body: JSON.stringify(data),
            };

            try {
                const response = await fetch(`${apiUrl}/words/api/${id}/aze/${currentQuestion}/${sessionId}/val/run`, requestOptions);
                if (!response.ok) {
                    throw new Error("Network response was not ok");
                }
                const responseData = await response.json();
                
                return responseData
            } catch (error) {
                // Handle errors
                console.error("There was a problem with the request:", error);
                //window.location.reload()
                return false; // Return false in case of error
        } finally {

        }
}


async function updateResultToData(correctOrNot) {
    const id = getUserIdFromUrl();
    const token = getToken();

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            Authorization: `${token}`,
        },
    };

    // Make the POST request
    return fetch(`${apiUrl}/words/api/validate/${id}/${correctOrNot}`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then((data) => {
            // Handle the response data
            return data.sessionID;
        })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
            throw error;
        })
        .finally(() => { });
}

function populateQuizWithQuestion(data) {
    const questionContainer = document.getElementById("questionContainer");
    const dropTarget = document.getElementById("dropTarget");
    const question = document.getElementById("question");

    //dropTarget.style.display = "block";

    // Clear previous alternatives
    questionContainer.innerHTML = "";
    question.style.color = "0";
    question.innerHTML = "";

    question.style.margin = "50px";

    // Create draggable alternatives
    data.alternatives.forEach((alternative, index) => {
        let draggableItem = document.createElement("div");
        draggableItem.classList.add("draggable");
        draggableItem.draggable = true;
        draggableItem.textContent = ` ${alternative}`;
        draggableItem.setAttribute("data-index", index);
        draggableItem.id = alternative;

        addClickListener(draggableItem, dropTarget);
        addTouchClickListener(draggableItem, dropTarget);

        questionContainer.appendChild(draggableItem);

        // Add click event listener to return item to its original position
        draggableItem.addEventListener("click", (event) => {
            const dataIndex = event.target.getAttribute("data-index");
            const draggedItem = document.querySelector(`[data-index="${dataIndex}"]`);

            if (draggedItem) {
                questionContainer.appendChild(draggedItem);
            }
        });
    });

    // Add drop event listener to the drop target
    dropTarget.addEventListener("drop", (event) => {
        event.preventDefault();
        const dataIndex = event.dataTransfer.getData("text/plain");
        const draggedItem = document.querySelector(`[data-index="${dataIndex}"]`);

        if (draggedItem) {
            dropTarget.appendChild(draggedItem);
        }
    });

    dropTarget.addEventListener("dragover", (event) => {
        event.preventDefault();
    });
    
    // timer
    const delay = 50; // 2000 milliseconds = 2 seconds
    const timerId = setTimeout(() => {
        hideLoadingScreen()
    }, delay);
}

function populateHealth(health) {
    if (health !== 0) {
        const div = document.getElementById("health");
        div.innerHTML = "";

        var span = document.createElement("span");
        span.style.fontSize = "25px";
        span.textContent = `${health}`;
        div.appendChild(span);
    } else {
        ranOutOfHealth();
    }
}

function ranOutOfHealth() {
    let id = getUserIdFromUrl();
    let sessionId = getCurrentActiveSession();
    const url = new URL(window.location.href);
    const newUrl = `${url.origin}/${id}/quiz/${sessionId}/no_health`;
    window.location.href = newUrl;
}

function youAreCorrect() {
    var nextButtonFront = document.getElementById("nextButtonFront")
    var nextButtonEdge = document.getElementById("nextButtonEdge")
    nextButtonFront.classList.remove("front-red")
    nextButtonEdge.classList.remove("edge-red")

    let popup = document.getElementById("popup");
    popup.style.background = "";
    let correctContainer = document.getElementById("correctContainer");
    correctContainer.style.display = "block";
    document.getElementById("incorrectContainer").style.display = "none";
    if (!document.getElementById("correctHeader")) {
        let header = document.createElement("h2")
        header.id = "correctHeader"
        header.innerHTML = "Correct!!"
        correctContainer.appendChild(header)
    }
    popup.style.display = "block";
}

function youAreIncorrect(answer) {
    var mode = getCookie("mode")
    var nextButtonFront = document.getElementById("nextButtonFront")
    var nextButtonEdge = document.getElementById("nextButtonEdge")
    nextButtonFront.classList.add("front-red")
    nextButtonEdge.classList.add("edge-red")

    if (mode == "drag_drop") {
        answer = document.getElementById("dropTarget").textContent.trim()
    }

    getCurrentQuestion().then((current) => {
        getCorrectAnswer(current).then((correct) => {
            getHealth().then((health) => {
                populateHealth(health);
            });

            let popup = document.getElementById("popup");
            popup.style.background = red;
            let incorrectContainer = document.getElementById("incorrectContainer");
            incorrectContainer.style.display = "block";
            document.getElementById("correctContainer").style.display = "none";
            if (!document.getElementById("incorrectHeader")) {
                let header = document.createElement("h2")
                header.id = "incorrectHeader"
                header.innerHTML = `Incorrect: correct answer is ${correct}`
                incorrectContainer.appendChild(header)
            } else {
                let header = document.getElementById("incorrectHeader")
                header.innerHTML = `Incorrect: correct answer is ${correct}`
            }
            popup.style.display = "block";
        });
    });

}

function lastQuestion() {
    let id = getUserIdFromUrl();
    let sessionId = getCurrentActiveSession();
    const url = new URL(window.location.href);
    const newUrl = `${url.origin}/${id}/quiz/${sessionId}/end`;
    deleteCookie("last");
    window.location.href = newUrl;
}

// Loads the result if you reach the end
function loadResult() {
    let lastPath = getLastPathSegment();
    if (lastPath == "end") {
        loadInResultView()
    }
}

function nextQuestion() {
    document.getElementById("answerWrite").value = "";
    const dropTarget = document.getElementById("dropTarget");
    const checkButton = document.getElementById("checkButton");
    const response = document.getElementById("response");
    const popup = document.getElementById("popup");
    popup.style.display = "none";

    dropTarget.innerHTML = "";
    response.innerHTML = "";

    checkButton.style.display = "block";

    loadWords();
}

async function fetchAnswer() {
    const mode = getCookie("mode")
    var answer = "";

    switch (mode) {
    case "drag_drop":
        dragAnswer = document.getElementById("dropTarget").textContent.trim()
            evaluateAnswerRequest(dragAnswer).then((response) => {
                if (response.last) {
                    setCookie("last", response.last, 1)
                }

                if (response.correct) {
                    youAreCorrect()
                } else if  (response.incorrect) {
                    youAreIncorrect(dragAnswer)
                };

            });
    case "write":
        answer = document.getElementById("answerWrite").value.trim()
            evaluateAnswerRequest(answer.trim()).then((response) => {
                if (response.last) {
                    setCookie("last", response.last, 1)
                }

                if (response.correct) {
                    youAreCorrect()
                } else if  (response.incorrect) {
                    youAreIncorrect(answer.trim())
                };
            });
    }



}

function addClickListener(draggableItem, dropTarget) {
    // Add mousedown event listener to start tracking
    draggableItem.addEventListener("mousedown", (event) => {
        startX = event.clientX;
        startY = event.clientY;
        draggableItem = event.target;
    });

    // Add mouseup event listener to end tracking and move item
    draggableItem.addEventListener("mouseup", (event) => {
        const endX = event.clientX;
        const endY = event.clientY;

        // Check if the movement is significant enough to consider it a drag
        if (Math.abs(endX - startX) > 5 || Math.abs(endY - startY) > 5) {
            return; // Not a click, it's a drag
        }

        const dataIndex = event.target.getAttribute("data-index");
        const itemToMove = questionContainer.querySelector(
            `[data-index="${dataIndex}"]`,
        );

        if (itemToMove) {
            const dragContainer = document.createElement("div");
            dragContainer.style.display = "inline-block";
            itemToMove.style.display = "inline-block";
            itemToMove.style.marginTop = "0";
            if (isMobileDevice()) {
                itemToMove.style.marginTop = "10px";
            }
            // Apply slide animation class
            itemToMove.classList.add("slide");
            setTimeout(() => {
                // Remove slide animation class after animation is complete
                itemToMove.classList.remove("slide");
            }, 200); // 0.3s (same as transition duration)
            dragContainer.appendChild(itemToMove);
            dropTarget.appendChild(itemToMove);
        }
    });
}

function addTouchClickListener(draggableItem, dropTarget) {
    // Add touchstart event listener to start tracking
    draggableItem.addEventListener("touchstart", (event) => {
        startX = event.touches[0].clientX;
        startY = event.touches[0].clientY;
        draggableItem = event.target;
    });

    // Add touchend event listener to end tracking and move item
    draggableItem.addEventListener("touchend", (event) => {
        const endX = event.changedTouches[0].clientX;
        const endY = event.changedTouches[0].clientY;

        // Check if the movement is significant enough to consider it a drag
        if (Math.abs(endX - startX) > 5 || Math.abs(endY - startY) > 5) {
            return; // Not a click, it's a drag
        }

        const dataIndex = event.target.getAttribute("data-index");
        const itemToMove = questionContainer.querySelector(
            `[data-index="${dataIndex}"]`,
        );

        if (itemToMove) {
            itemToMove.style.display = "inline-block";
            itemToMove.style.marginTop = "0";
            if (isMobileDevice()) {
                itemToMove.style.marginTop = "10px";
            }

            dropTarget.appendChild(itemToMove);
        }
    });
}


async function getSessionId() {
    const id = getUserIdFromUrl();
    const token = getToken();

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            Authorization: `${token}`,
        },
    };

    // Make the POST request
    return fetch(`${apiUrl}/account/${id}`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then((data) => {
            // Handle the response data
            return data.sessionID;
        })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
            throw error;
        })
        .finally(() => { });
}

function retrieveSessionData() {
    var sessionId = getCurrentActiveSession()
    var data = getSessionData(sessionId)
    return data;
}


async function retrieveEndResult() {
    return getSessionId().then(async (sessionID) => {
        const data = await getSessionData(sessionID);
        return data;
    });
}

async function getResultCorrect() {
    return retrieveSessionData().then((session) => {
        return session.correctAnswers;
    });
}

async function getResultIncorrect() {
    return retrieveSessionData().then((session) => {
        return session.incorrectAnswers;
    });
}

async function getCurrentQuestion() {
    return retrieveSessionData().then((session) => {
        return session.currentQuestion;
    });
}

async function getCorrectAnswer(current) {
    return retrieveSessionData().then((session) => {
        return session.sentences[current - parseInt(1)]["correct"];
    });
}

async function getHealth() {
    return retrieveSessionData().then((session) => {
        return session.health;
    });
}

function hideDragDropMode() {
    const dropTarget = (document.getElementById("dropTarget").style.display =
        "none");
    const questionContainer = (document.getElementById(
        "questionContainer",
    ).style.display = "none");
}

function showDragDropMode() {
    const dropTarget = (document.getElementById("dropTarget").style.display =
        "block");
    const questionCOntainer = (document.getElementById(
        "questionContainer",
    ).style.display = "block");
}

function hideWriteMode() {
    const writeBox = (document.getElementById("writeBox").style.display = "none");
}

function showWriteMode() {
    const writeBox = (document.getElementById("writeBox").style.display =
        "block");
}

function getCurrentActiveSession() {
    return window.location.href.split("/")[5]
}


function isMobileDevice() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent,
    );
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

function getGameMode(int) {
    switch (int) {
        case 1:
            var gameMode = "drag";
            return gameMode;
        case 2:
            var gameMode = "write";
            return gameMode;
        default:
            console.Error("Mode not found");
    }
}

// Runs when page load and add click listeners to the buttons
// on the quiz page
document.addEventListener("DOMContentLoaded", function() {
    const checkButton = document.getElementById("checkButton");
    const checkButtonAlt = document.getElementById("altCheckButton");
    const nextButton = document.getElementById("nextButton");
    const homeButton = document.getElementById("homeButton");

    const screenWidth = window.innerWidth;
    if (screenWidth < 768) {
        if (nextButton) {
            nextButton.style.width = "100%";
        }
    }

    if (checkButton) {
        checkButton.addEventListener("click", function() {
            const questionContainer = document.getElementById("questionContainer");

            fetchAnswer();

            // clear the question container
            questionContainer.innerHTML = "";

            // hide the drop target
            dropTarget.style.display = "none";
            checkButton.style.display = "none";
            nextButton.style.display = "block";

        });

    }
    
    if (checkButtonAlt) {
        checkButtonAlt.addEventListener("click", function() {
            const questionContainer = document.getElementById("questionContainer");

            fetchAnswer();

            // clear the question container
            questionContainer.innerHTML = "";

            // hide the drop target
            dropTarget.style.display = "none";
            checkButton.style.display = "none";
            nextButton.style.display = "block";

        });

    }
    
    if (nextButton) {
        nextButton.addEventListener("click", function() {
            nextQuestion();
        });
    }
    
    if (homeButton) {
        homeButton.addEventListener("click", function() {
            //updateSessionCompleteStatus();
        });

    }



});


document.addEventListener("htmx:afterSettle", function(event) {
    var closeButton = document.getElementById("closeButton")
    if (closeButton) {
        closeButton.addEventListener("click", function() {
            var success = document.getElementById("message").innerHTML = "";
        });
    }

    loadWords()
    loadResult()
});


async function fetchImage(question) {
    // Create the request options, including the method, headers, and body
    showLoadingScreen()
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            "Question": `${question}`,
        },
    };
    
    // Make the POST request
    return fetch(`/get/image`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then((image) => {
            // Handle the response data
            hideLoadingScreen()
            return image;
        })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
            throw error;
        })
        .finally(() => { });
  
}

function updateProgress(percentage) {
    const progressBar = document.getElementById("progress-bar");
    progressBar.style.width = percentage + "%";
}

document.addEventListener('htmx:load', function(event) {
          var image = document.getElementById('image');
          var response = event.detail.response;
          if (response && response.imageUrl) {
              image.src = response.imageUrl;
          }
      });

document.addEventListener("DOMContentLoaded", loadWords);
document.addEventListener("DOMContentLoaded", loadResult);


function loadInResultView() {
    const correctWordContainer = document.getElementById("correctWordContainer")
    const incorrectWordContainer = document.getElementById("incorrectWordContainer")
    const resultButton = document.getElementById("resultButton");
    const endButton = document.getElementById("endButton");
    const resultWordContainer = document.getElementById("resultWordContainer");

    endButton.addEventListener("click", function() {
        window.location.href = "/get/menu/Stage1";
    });
    resultButton.addEventListener("click", function() {
        resultWordContainer.classList.toggle("hide");
        resultWordContainer.classList.toggle("show");
        

        if ( resultWordContainer.style.display == "block") {
            resultWordContainer.style.display = "none";
        } else {
            resultWordContainer.style.display = "block";
        }
    });
    retrieveSessionData().then((data) => {
        var correct = 0
        var incorrect = 0
        for (let i = 1; i <= data.maxAmountWords; i++) {
            var success = data.sentences[i]?.["success"] ?? 2;
            if (success == 1) {
                correct = parseInt(correct) + 1
            } else if (success == 0) { 
                incorrect = parseInt(incorrect) + 1
            } 
        }

        updateProgress(`${data.currentQuestion / data.maxAmountWords * 100}`)

        var correctWords = []
        var incorrectWords = []
        for (let i = 1; i <= data.maxAmountWords; i++) {
            const sentences = data.sentences[i];
            var p = document.createElement("p")
            var success = data.sentences[i]?.["success"] ?? 2;

            if (success == 1) {
                correctWords.push(sentences["question"])
                p.textContent = `${i} ${sentences["question"]}`
                correctWordContainer.appendChild(p)
            } else if (success == 0){
                incorrectWords.push(sentences["question"])
                p.textContent = `${i} ${sentences["question"]} - Correct: ${sentences["correct"]}
                    - Answer: ${sentences["userAnswer"]}`
                incorrectWordContainer.appendChild(p)
            }
        }

        if (incorrectWords.length == 0) {
            incorrectWordContainer.style.display = "none";
        }

        if (correctWords.length == 0) {
            correctWordContainer.style.display = "none";

        }

        // correct container
        let divCorrect = document.getElementById("correctContainer");
        divCorrect.innerHTML = "";
        const correctElement = document.createElement("h2");
        correctElement.textContent = `Correct: ${correct}`;
        correctElement.classList.add("correct-mes");
        correctElement.classList.add("margin");
        divCorrect.appendChild(correctElement);

        // incorrect container
        let divInCorrect = document.getElementById("incorrectContainer");
        divInCorrect.innerHTML = "";
        const incorrectElement = document.createElement("h2");
        incorrectElement.textContent = `Incorrect: ${incorrect}`;
        incorrectElement.classList.add("incorrect-mes");
        incorrectElement.classList.add("margin");
        divInCorrect.appendChild(incorrectElement);
    });

}

