function getLastPathSegment() {
    // Get the full pathname from the URL
    var fullPathname = window.location.pathname;

    // Split the pathname by '/' to get individual segments
    var pathSegments = fullPathname.split("/");

    // Get the last segment from the array
    var lastPathSegment = pathSegments[pathSegments.length - 1];

    return lastPathSegment;
}
