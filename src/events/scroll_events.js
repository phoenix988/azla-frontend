document.addEventListener("DOMContentLoaded", function() {
    const stage1Header = document.getElementById("stage1Header");
    const allHeaders = document.querySelectorAll(".stage-headers");
    const navHeader = document.getElementById("nav");
    const headerTop = document.getElementById("headerTop");
    const banner = document.getElementById("banner");
    const footer = document.getElementById("footer");
    const navbar = document.getElementById("navbar");

    var stage1HeaderOffSet ;
    var footerOffSet ;

    if (stage1Header) {
        stage1HeaderOffSet = stage1Header.offsetTop;
    }

    if (footer) {
        footerOffSet = footer.offsetTop;
    }

    let previousStates = [];
    let previousStateIndex = -1;

    // Function to handle scroll events on Stage1 header
    function handleStage1Header() {
        const scrollPosition = window.scrollY;
        const stageFooter = document.getElementById("stageFooter");

        if (scrollPosition >= footerOffSet) {
            stage1Header.classList.remove("header-top"); 
            navHeader.classList.remove("white-background"); 
        }
        else if (scrollPosition >= stage1HeaderOffSet) {
            stage1Header.classList.add("header-top"); 
        } else {
            stage1Header.classList.remove("header-top"); 
            headerTop.classList.add("header-top");
            stageFooter.style.display = "none";
        }
    }

    // Function to handle scroll events
    function handleScroll() {
        allHeaders.forEach((header, index) => {
            var stageHeaderOffset = header.offsetTop;
            const stageFooter = document.getElementById("stageFooter");
            var stageFooterOffSet = stageFooter.offsetTop;

            const scrollPosition = window.scrollY;
            // Determine if header is within viewport
            const isWithinViewport = scrollPosition >= stageHeaderOffset;

            // Check if the state has changed since the last scroll
            if (previousStates[index] !== isWithinViewport) {
                if (isWithinViewport) {
                    header.classList.add("header-top");
                    headerTop.classList.remove("header-top");
                } else {
                    header.classList.remove("header-top");
                }

                // Update the previous state for the current header
                previousStates[index] = isWithinViewport;

                // Remove the class from the previous header if it exists
                if (previousStateIndex !== -1 && previousStateIndex !== index) {
                    allHeaders[previousStateIndex].classList.remove("header-top");
                    previousStates[previousStateIndex] = false;
                }

                // Update the index of the previous affected header
                previousStateIndex = index;

                if (scrollPosition >= footerOffSet) {
                    header.classList.remove("header-top")
                    header.style.display = "none";
                }
            }
        });
    }


    function handleQuiz() {
        const scrollPosition = window.scrollY;
        const navbar = document.getElementById("navbar");
        const progressBar = document.getElementById("progressBarContainer")
        if (scrollPosition >= footerOffSet) {
            if (progressBar) {
                progressBar.style.display = "none";
            }

            navbar.classList.remove("header-top");
        } else {
            if (progressBar) {
                progressBar.style.display = "block";
            }
            navbar.classList.add("header-top");
        }
    }

    var bannerOffset;
    if (banner) {
        bannerOffset = banner.offsetTop;
    }
    // Function to handle scroll event for both units
    function handleBanner() {
        const scrollPosition = window.scrollY;

        if (scrollPosition >= bannerOffset) {
            navHeader.classList.add("white-background"); 
        } else {
            navHeader.classList.remove("white-background"); 
        }
    }

    // Add event listener for scroll event
    if (stage1Header) {
        window.addEventListener("scroll", handleStage1Header);
    }

    if (banner) {
        window.addEventListener("scroll", handleBanner);
    }

    window.addEventListener("scroll", handleScroll);

    if (navbar) {
        window.addEventListener("scroll", handleQuiz);
    }
});


//document.addEventListener("DOMContentLoaded", function() {
//    // Select the elements to observe
//    const stageFooter = document.getElementById("stageFooter");
//    const footer = document.getElementById("footer");
//    const firstStageBottom = document.getElementById("firstStageBottom");
//
//
//    // Create the IntersectionObserver instance
//    const observer = new IntersectionObserver(entries => {
//        entries.forEach(entry => {
//            if (entry.target.id === "footer" && entry.isIntersecting) {
//                if (stageFooter) {
//                    stageFooter.style.display = "none";
//                }
//            }
//        });
//    });
//
//
//    // Create the IntersectionObserver instance
//    const observer1 = new IntersectionObserver(entries => {
//        entries.forEach(entry => {
//            if (entry.target.id === "firstStageBottom" && entry.isIntersecting) {
//                stageFooter.style.display = "block";
//            }         
//        });
//    });
//
//
//    // Observe the stageFooter and footer elements
//    if (stageFooter) {
//        observer.observe(stageFooter);
//    }
//    if (footer) {
//        observer.observe(footer);
//    }
//
//    if (firstStageBottom) {
//        observer1.observe(firstStageBottom);
//    }
//    if (stageFooter) {
//        observer1.observe(stageFooter);
//    }
//
//});
//
//document.addEventListener("DOMContentLoaded", function() {
//  const allHeaders = document.querySelectorAll(".stage-headers");
//  const footer = document.getElementById("footerHeader");
//
//  // Function to check if the scroll position has reached the footer
//  function isFooterVisible() {
//    const windowHeight = window.innerHeight;
//    var footerRect;
//    var footerTop;
//    if ( footer ) {
//        footerRect = footer.getBoundingClientRect();
//    }
//    if (footerRect) {
//        footerTop = footerRect.top;
//    }
//
//    // Calculate the distance between the top of the footer and the bottom of the viewport
//    const distanceToFooter = footerTop - windowHeight;
//
//    // Check if the distance to the footer is less than or equal to zero
//    return distanceToFooter <= 0;
//  }
//
//  // Function to handle scroll events
//  function handleFooter() {
//    if (isFooterVisible()) {
//      // Scroll position has reached the footer
//      // Perform your desired action here
//      allHeaders.forEach((header) => {
//        header.classList.remove("header-top");
//
//      });
//    }
//  }
//
//  // Add scroll event listener to the window
//  window.addEventListener("scroll", handleFooter);
//});
