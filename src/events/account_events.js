function saveToken(token) {
    // Check if localStorage is available
    if (typeof localStorage !== "undefined") {
        try {
            // Save the token securely in local storage
            localStorage.setItem("token", token);
        } catch (error) {
            // Handle localStorage errors
            console.error("Error saving token:", error);
        }
    } else {
        // Handle unsupported localStorage
        console.error("LocalStorage is not supported in this browser.");
    }
}

function removeToken() {
    localStorage.removeItem("token");
}

function getToken() {
    // Check if localStorage is available
    if (typeof localStorage !== "undefined") {
        try {
            // Retrieve the token from local storage
            const token = localStorage.getItem("token");
            if (token !== null) {
                // Token found, return it
                return token;
            } else {
                // Token not found in local storage
                console.error("Token not found in local storage.");
                return null;
            }
        } catch (error) {
            // Handle localStorage errors
            console.error("Error retrieving token:", error);
            return null;
        }
    } else {
        // Handle unsupported localStorage
        console.error("LocalStorage is not supported in this browser.");
        return null;
    }
}

async function getUserProfile(username) {
    try {
        const token = getToken();
        // Make the POST request
        const response = await fetch(`${apiUrl}/email/${username}`, {
            method: "GET",
            headers: {
                Authorization: `${token}`,
                "Content-Type": "application-json",
            },
        });

        if (!response.ok) {
            throw new Error("Network response was not ok");
        }

        const data = await response.json(); // Parse the JSON response
        return data;
    } catch (error) {
        // Handle errors
        console.error("There was a problem with the request:", error);
        throw error; // Re-throw the error to handle it elsewhere if needed
    }
}

async function getUserProfileById(id) {
    try {
        // Make the POST request
        const token = getToken();

        // Make the POST request
        const response = await fetch(`${apiUrl}/account/${id}`, {
            method: "GET",
            headers: {
                Authorization: `${token}`,
                "Content-Type": "application-json",
            },
        });

        if (!response.ok) {
            throw new Error("Network response was not ok");
        }

        const data = await response.json(); // Parse the JSON response
        return data;
    } catch (error) {
        // Handle errors
        console.error("There was a problem with the request:", error);
        throw error; // Re-throw the error to handle it elsewhere if needed
    }
}

async function getUserProfileByUserName(username) {
    try {
        // Retrieve the token from local storage
        const token = getToken();

        // Check if token is available
        if (!token) {
            throw new Error("Token not available");
        }

        // Make the POST request
        const response = await fetch(`${apiUrl}/username/${username}`, {
            method: "GET",
            headers: {
                Authorization: `${token}`,
                "Content-Type": "application-json",
            },
        });

        if (!response.ok) {
            throw new Error("Network response was not ok");
        }

        const data = await response.json(); // Parse the JSON response
        return data;
    } catch (error) {
        // Handle errors
        console.error("There was a problem with the request:", error);
        throw error; // Re-throw the error to handle it elsewhere if needed
    }
}


async function getAllSessions() {
    const token = getToken()
    const id = getUserIdFromUrl()
    const requestOptions = {
        method: "GET",
        headers: {
            Authorization: `${token}`,
            "Content-Type": "application-json",

        },
    }

    return fetch(`${apiUrl}/account/${id}/sessions`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        })
        .then((data) => {
            return data
        })
        .catch((error) => {
            // Handle errors
            console.error("There was a problem with the request:", error);
        })
        .finally(() => {
        });


}

function getUserIdFromUrl() {
    var pathname = window.location.pathname;
    var pathSegments = pathname.split("/");

    // Get the first path segment after the domain
    var id = pathSegments[1];

    return id;
}

function signOut() {
    deleteCookie("username");
    deleteCookie("id");
    removeToken();
    window.location.href = "/login";
}

function toProfile() {
    var id = getCookie("id");
    const url = new URL(window.location.href);
    const newUrl = `${url.origin}/${id}/profile`;
    window.location.href = newUrl;
}

function toMenu() {
    var id = getCookie("id");
    const url = new URL(window.location.href);
    const newUrl = `${url.origin}/${id}/menu`;
    window.location.href = newUrl;
}


function saveTokenToStorage() {
    // Create a data object with the body of your POST request
    const token = getToken()

    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
    };

    // Make the POST request
    fetch(`/get/token`, requestOptions)
        .then((response) => {
            if (!response.ok) {
                throw new Error("Network response was not ok");
            }
            return response.json(); // Parse the JSON response
        }). then((data) => {
            saveToken(data)
        });
}
