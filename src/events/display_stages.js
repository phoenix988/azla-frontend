const allStages = ["Stage1", "Stage2", "Stage3"]


document.addEventListener("DOMContentLoaded", function() {
    var nextStage = document.querySelectorAll(".next-stage")
    var prevStage = document.querySelectorAll(".prev-stage")

    nextStage.forEach((next) => {
        next.addEventListener("click", function() {
            var newStage = parseInt(next.id.split(" ")[1]) + 1
            var check = checkIfStageExist(newStage)

            if (check) {
                //window.location.href = `Stage${newStage}`
                window.history.pushState({}, '', `Stage${newStage}`)
                
                showSelectedStage()
            }

        });
    });


    prevStage.forEach((prev) => {
        prev.addEventListener("click", function() {
            var newStage = parseInt(prev.id.split(" ")[1]) - 1
            var check = checkIfStageExist(newStage)

            if (check) {
                window.history.pushState({}, '', `Stage${newStage}`)
                
                showSelectedStage()
                
            }
        });
    });

});


function showSelectedStage() {
    hideAllStages()
    var choosenStage = window.location.href.split("/")[6].split("#")[0]
    var stage = document.getElementById("section1"+choosenStage)

    if (stage) {
        stage.style.display = "block";
    }
}

function hideAllStages() {
    allStages.forEach((stage) => {
        const id = document.getElementById("section1"+stage);
        if (id) {
            id.style.display = "none";
        }
    });
}


function checkIfStageExist(stage) {
    const checkStage = document.getElementById("section1Stage"+stage);
    if (!checkStage) {
        return false
    }

    return true
}


document.addEventListener("DOMContentLoaded", showSelectedStage())
