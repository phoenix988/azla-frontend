// JavaScript
let currentIndexBut = 0;
let currentBubble = 0;
let currentJump = 0;
let currentDrop = 0;
let currentDropArrow = 0;
let currentIndexRing = 0;

// check for changes
var check = 0 

// all stages available
var sections = ["section1"];
var stages = ["Unit1", "Unit2", "Unit3"];
var level = [1, 2, 3, 4, 5];
var lessons = [1, 2, 3, 4];
var checkStage = {};

const positions = [
    -100, // Define the position for the first button
    0, // Define the position for the second button
    100, // Define the position for the third button
    0,
    -100,
    // Add more positions as needed
];

var count = 0;


function initializePage() {
    var sectionContainer = document.querySelectorAll(".section-container")

    sectionContainer.forEach((section, index) => {
        count = parseInt(count) + parseInt(1)
        var keepTrack = {
          currentStage: `Unit${count}`,
          currentStageSmall: `unit${count}`,
          currentStageAlt: `Stage${count}`,
          currentStageAltSmall: `stage${count}`,
          count: `${count}`,
        }
        createStageMenu(section, keepTrack)
    });

    checkForProgress()

    const timer = setTimeout(function() {
        // This code will run after the timer expires
        hideLoadingScreen()
        // Add your code here that you want to run after the timer expires
    }, 300); // 5000 milliseconds = 5 seconds
}

let isPageLoaded = false;

document.addEventListener("DOMContentLoaded", function() {
    // Set the flag to true indicating that the page is initially loaded
    isPageLoaded = true;
    
    initializePage();

    setCurrentProgress()

    windowClickEvent()
    
    let button = document.getElementById("profileDropdownBtn")
    toggleDropProfile(button)
});


document.addEventListener("htmx:afterSettle", function() {
    windowClickEvent()
});

// Listen for popstate event to detect navigation
window.addEventListener("popstate", function(event) {
    // Check if the page is initially loaded
    if (isPageLoaded) {
        // Page is initially loaded, do nothing
        // You may add additional logic here if needed
    } else {
        // Page is not initially loaded, handle navigation
        // Re-initialize the page if necessary
        initializePage();
    }
});

function getLanguage() {
    return window.location.href.split("/")[5]
}

function checkForProgress() {
    showLoadingScreen()
    var last = ""; let currentLvl; var numArray = []
    var unlockedStagesAndLevels = {}
    var language = getLanguage()
    const promises = [];

    stages.forEach((stage) => {
        numArray = []
        unlockedStagesAndLevels[stage] = {}
        level.forEach((lvl) => {
            var previousLesson = 0
            unlockedStagesAndLevels[stage][lvl] = {}
            lessons.forEach((lsn) => {
                const promise = loadProgress(language).then((progress) => {
                    const lessonLabel = document.getElementById(stage+"level"+lvl+"Lesson")
                    if (progress.level.progress[stage][lvl][lsn]) {
                        currentLvl = lvl
                        const subButton = document.querySelector(`button[name="${stage} Level${lvl}"]`);

                        addButtonClickAction(subButton)
                        if (lsn > previousLesson && lsn !== 4) {
                            lessonLabel.textContent = `Lesson ${lsn} / 3`
                        }
                        numArray.push(stage+lvl)

                        previousLesson = lsn

                        unlockedStagesAndLevels[stage][lvl][lsn] = {"done": true}
                    } 

                }).catch(() => {
                });
                promises.push(promise);
            });
        });
    });

    // Wait for all promises to resolve
    Promise.all(promises).then(() => {
        //var largest = getLargestNumber(numArray)
        var done = {} // checks for completed stages
        var highest = 0
        numArray.forEach((array) => {
            var seperate = array.split("")
            var check = seperate[4]+seperate[5]

            if (check > highest) {
                highest = seperate[4]+seperate[5]
            }
        });
        let stageAndLevel
        if (stageAndLevel) {
            stageAndLevel = highest.split("")
        } else {
            var num = "11"
            stageAndLevel = num.split("")
        }
        
        last = "startUnit"+stageAndLevel[0]+"Level"+currentLvl;
        jump = "jumpUnit"+stageAndLevel[0]+"Level"+stageAndLevel[1];
        
        document.getElementById(last).style.display = "block";
        document.getElementById(jump).style.display = "none"; // make sure jump bubble is hidden

        stages.forEach((stage) => {
            level.forEach((lvl) => {
                lessons.forEach((lsn) => {
                    if (unlockedStagesAndLevels[stage][lvl][lsn]) {
                        done[stage] = lvl
                        if (lsn == 4) {
                            const button = document.querySelector(`button[name="${stage} ${lvl}"]`);
                            const checkMark = document.getElementById(`check${stage}Level${lvl}`)
                            if (checkMark) {
                                checkMark.style.display = "block";
                            }

                            setBackgroundColor(button, "100%")
                        } else if (lsn == 3) {
                            const button = document.querySelector(`button[name="${stage} ${lvl}"]`);

                            setBackgroundColor(button, "66%")
                        } else if (lsn == 2) {
                            const button = document.querySelector(`button[name="${stage} ${lvl}"]`);

                            setBackgroundColor(button, "33%")
                        } else if (lsn == 1) {
                            const button = document.querySelector(`button[name="${stage} ${lvl}"]`);

                            setBackgroundColor(button, "0%")
                        }
                    } else {
                        if (lvl == 1) {
                            const jumpBubble = document.getElementById(`jump${stage}Level${lvl}`)
                            const startBubble = document.getElementById(`start${stage}Level${lvl}`)
                            const subButton = document.querySelector(`button[name="${stage} Level${lvl}"]`);

                            if (startBubble.style.display !== 'block') {
                                jumpBubble.style.display = 'block';
                                addButtonClickActionSkip(subButton)
                            } else {
                                addButtonClickAction(subButton)
                            }
                        } else if (done[stage] !== lvl) {
                            
                            const button = document.querySelector(`button[name="${stage} ${lvl}"]`);
                            lockQuestions(button)
                            lockQuestionsDropDown(stage, "Level"+lvl)

                        }

                    }
                });
            });
        });
    });
}

function createDropDownMenuArrows() {
    // Get all elements with the class dropdown-content
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var dropdownsLocked = document.getElementsByClassName("dropdown-content-locked");

    // Loop through each dropdown element
    for (var i = 0; i < dropdowns.length; i++) {
        // Get the computed style of the ::after pseudo-element
        var afterStyle = window.getComputedStyle(dropdowns[i], '::after');
        var positionVertical

        let position = positions[currentDropArrow];
        currentDropArrow = (currentDropArrow + 1) % positions.length;
        var positionLeft = dropdowns[i].offsetWidth < 200 ? `${position+200}px` : '20px';


        if (window.innerWidth < 600) {
            var positionVertical = dropdowns[i].offsetWidth < 200 ? `180px` : '20px';
            var positionLeft = dropdowns[i].offsetWidth < 200 ? `${position+170}px` : '20px';
        } else if (window.innerWidth < 768) {
            var positionVertical = dropdowns[i].offsetWidth < 200 ? `180px` : '20px';
        } else if (window.innerWidth < 1000) {
            var positionVertical = dropdowns[i].offsetWidth < 200 ? `185px` : '20px';

        } else if (window.innerWidth < 1400) {
            var positionVertical = dropdowns[i].offsetWidth < 200 ? `185px` : '20px';
        } else {
            var positionVertical = dropdowns[i].offsetWidth < 200 ? `190px` : '20px';
        }

        // Set the CSS style for the left position of the ::after pseudo-element
        dropdowns[i].style.setProperty('--arrow-left', positionLeft);
        dropdowns[i].style.setProperty('--arrow-vertical', positionVertical);
    }

}


function positionButtons() {
    const buttons = document.querySelectorAll(".d-btn-lvl");
    const startBubble = document.querySelectorAll(".chat-bubble-start");
    const jumpBubble = document.querySelectorAll(".chat-bubble-jump");
    const dropdown = document.querySelectorAll("dropdown-content");
    const rings = document.querySelectorAll(".ring-container");

    buttons.forEach((button) => {
        let position = positions[currentIndexBut];
        currentIndexBut = (currentIndexBut + 1) % positions.length;

        button.style.left = position + "px";
    });

    rings.forEach((ring) => {
        let position = positions[currentIndexRing];
        currentIndexRing = (currentIndexRing + 1) % positions.length;

        ring.style.left = position + "px";
    });

    // Add click event listener to each anchor
    startBubble.forEach((bubble) => {
        let position = positions[currentBubble];
        currentBubble = (currentBubble + 1) % positions.length;

        bubble.style.left = position + "px";
        bubble.style.marginBottom = "30px";
    });

    jumpBubble.forEach((bubble) => {
        let position = positions[currentJump];
        currentJump = (currentJump + 1) % positions.length;

        bubble.style.left = position + "px";
        bubble.style.marginBottom = "30px";
    });

    // Add click event listener to each anchor
    dropdown.forEach((drop) => {
        let position = positions[currentDrop];
        currentDrop = (currentDrop + 1) % positions.length;
        drop.style.left = position + "px";
    });
}


function toggleDropFlag(button) {
    // Add click event listener to each anchor
    button.addEventListener("click", function(event) {
        var dropdownsLang = document.getElementsByClassName("dropdown-language");
        var i;
        for (i = 0; i < dropdownsLang.length; i++) {
            var openDropdown = dropdownsLang[i];
                if (check === 0) {
                    openDropdown.style.display = "block";
                    slideInFromTop(openDropdown)
                    check = 1
                } else {
                    fadeOut(openDropdown)
                    check = 0
                }
            }
    });

    button.addEventListener("mouseenter", function(event) {
        var dropdownsLang = document.getElementsByClassName("dropdown-language");
        var i;
        for (i = 0; i < dropdownsLang.length; i++) {
            var openDropdown = dropdownsLang[i];
            if (openDropdown.style.display !== "block") {
                openDropdown.style.display = "block";
                slideInFromTop(openDropdown)
                check = 1
            }
            }
    });

}

function toggleDropProfile(button) {
    // Add click event listener to each anchor
    button.addEventListener("click", function(event) {
        let dropdownContent = document.getElementById("profileDropdown");

        if (dropdownContent) {
            if (dropdownContent.style.display == "" || dropdownContent.style.display == "none") {
                dropdownContent.style.display = "block";
                slideInFromTop(dropdownContent);
            } else {
                fadeOut(dropdownContent);
            }

        }
    });

}

function toggleDrop(button) {
    const checkMark = document.querySelectorAll(".check-mark");

    // Add click event listener to each anchor
    button.addEventListener("click", function(event) {
        let id = this.getAttribute("id");
        let dropdownContent = document.getElementById("dc" + id);
        var dropdowns = document.getElementsByClassName("dropdown-content");

        if (dropdownContent) {
            if (dropdownContent.style.display == "" || dropdownContent.style.display == "none") {
                dropdownContent.style.display = "block";
                slideInFromTop(dropdownContent);
            } else {
                fadeOut(dropdownContent);
            }

        }
    });

    checkMark.forEach((button) => {
        button.addEventListener("click", function(event) {
            let id = this.getAttribute("id");
        });
    });
}


function addButtonClickAction(button) {
    if (button) {
        button.addEventListener("click", function() {
            let name = this.getAttribute("name");
            let yourLevel = name.split(" ");

            startMainQuiz(yourLevel[0], yourLevel[1]);
        });
    }
}


function addButtonClickActionSkip(button) {
    if (button) {
        button.addEventListener("click", function() {
            let name = this.getAttribute("name");
            let yourLevel = name.split(" ");
            let stageNumber = yourLevel[0].split("");
            let num = 0
            stageNumber.forEach((number) => {
                num = number
            });

            startSkipToNextStage(`stage${num}`);
        });
    }
}

function createLinksForHeader(li, keepTrack) {
    var anchors = {
        a1: document.createElement("a"),
        a2: document.createElement("a"),
    }

    var icons = {
        i1: document.createElement("i"),
        i2: document.createElement("i")
    }
    
    anchors.a1.id = `prev ${keepTrack.count}`; anchors.a1.classList.add("prev-stage")
    anchors.a2.id = `next ${keepTrack.count}`; anchors.a2.classList.add("next-stage")
    icons.i1.classList.add("icon-size","nf","nf-fa-arrow_left")
    icons.i2.classList.add("icon-size","nf","nf-fa-arrow_right")

    anchors.a1.appendChild(icons.i1)
    anchors.a2.appendChild(icons.i2)
    anchors.a2.style.marginLeft = "4rem";


    var spans = {
        span1: document.createElement("span"),
        span2: document.createElement("span"),
    }

    li.li1.appendChild(spans.span1);
    li.li2.appendChild(spans.span2);

    spans.span1.textContent = "Section 1";
    var stage = seperateWord(`${keepTrack.currentStageAlt}`)
    spans.span2.textContent = `${stage.str} ${stage.num}`;


    return anchors
}

function createLiForHeader(ul, keepTrack) {
    var li = {
        li1: document.createElement("li"),
        li2: document.createElement("li"),
        li3: document.createElement("li"),
        li4: document.createElement("li"),
    }


    ul.ul1.appendChild(li.li1);
    ul.ul2.appendChild(li.li2);
    ul.ul2.appendChild(li.li3);
    ul.ul2.appendChild(li.li4);

    var anchors = createLinksForHeader(li, keepTrack)

    li.li3.appendChild(anchors.a1);
    li.li4.appendChild(anchors.a2);

    return ul
}

function createUlForHeader(keepTrack) {
    var ul = {
        ul1: document.createElement("ul"),
        ul2: document.createElement("ul"),
    }
    
    var ul = createLiForHeader(ul, keepTrack)


    return ul
}

function createNavBarForHeader(keepTrack) {
    const navBar = document.createElement("nav")
    navBar.classList.add("navbar")
    navBar.classList.add("fancy-navbar")
    navBar.classList.add("header-top")

    var ul = createUlForHeader(keepTrack)

    createLanguageSelectionList(navBar, ul)

    return navBar
}

function getFlagPaths() {
    var requestOptions = createRequestOptions("GET")
    var availableLang = {}
    sendFetchUrl(`${apiUrl}/get/language`, requestOptions).then((data) => {
        for (i = 0; i < data.length; i++) {
            availableLang[data[i]["language"]] = data[i]["flagpath"]
        }
    });

    return availableLang
}

var availableLang = getFlagPaths()

var switcher = {
    aze: function createLanguageSwitcher() {
        // image for dropdown button
        const img = document.createElement("img")
        img.src = "/assets/aze-flag.svg";
        img.classList.add("right-corner", "flagbtn"); img.style.width = "50px";
        img.id = "languageButton"
    
        return img
    },
    turk: function createLanguageSwitcher() {
        // image for dropdown button
        const img = document.createElement("img")
        img.src = "/assets/turk-flag.png";
        img.classList.add("right-corner", "flagbtn"); img.style.width = "50px";
        img.id = "languageButton"
    
        return img
    },
}

function selectChoosenTab(element) {
    clearSelectedTab()
    document.getElementById(element).classList.add("selected-tab")
    document.getElementById(element).style.color = whiteBackground;
    document.getElementById("levels").style.display = "none";
    slideInFromTop(document.getElementById("subview"))
    document.getElementById("subview").style.display = "block";
    document.getElementById("content").style.display = "block";
    document.getElementById("headerTitle").textContent = "Configuration";
}

function createLanguageSelectionList(navBar, ul) {
    const dropdownContent = document.createElement("div")
    dropdownContent.classList.add("dropdown-language","hide", "right-corner-0")
    dropdownContent.id = `flagDropDown`
    dropdownContent.style.marginTop = "70px"
    
    // image for dropdown button
    var img     
    var language = getLanguage()
        if (language == "azerbajani") {
            img = switcher["aze"]()
        } else if (language == "turkish") {
            img = switcher["turk"]()
        }
    // plus button to add languages
    const dropDownLang = document.createElement("span")
    dropDownLang.textContent = "+";
    dropDownLang.style.margin = "10px";
    dropDownLang.classList.add("text-color", "add-target");

    dropDownLang.addEventListener("click", function() {
          fetch('/update/configuration_menu', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(response => response.text()) // Assuming the server returns plain text or HTML
            .then(data => {
                document.getElementById('subview').innerHTML = data;
                selectChoosenTab("config")

                window.history.pushState({}, '', `#config`)

                document.addEventListener("DOMContentLoaded", createRemoveLanguageButton())
            })
            .catch(error => console.error('Error:', error));

    });
    // add click effect to toglge dropdown 
    toggleDropFlag(img)
    
    // append to the navbar
    navBar.appendChild(ul.ul1)
    navBar.appendChild(ul.ul2)
    navBar.appendChild(img)
    navBar.appendChild(dropdownContent)

    var availableLang = getFlagPaths()
    
    // load progress and add the images to it 
    loadProgressAll().then((progress) => {
        var check = {}
        progress.forEach((prog) => {
            
            switch (prog.language) {
            case "azerbajani":
                    if (!check[prog.language]) {
                        createDropDownMenuItem(prog, dropdownContent, check)
                    }
            case "turkish":
                    if (!check[prog.language]) {
                        createDropDownMenuItem(prog, dropdownContent, check)
                    }
                    
                    check[prog.language] = prog.language
            }
        });

    });

    dropdownContent.appendChild(dropDownLang)
    
}

function createDropDownMenuItem(prog, dropdownContent, check) {
    var dropDownImg = document.createElement("img")
    dropDownImg.src = availableLang[prog.language]; dropDownImg.style.width = "70px"; dropDownImg.style.margin = "10px";
    dropDownImg.style.height = "38px"
    dropdownContent.appendChild(dropDownImg)
    dropDownImg.classList.add("flagbtn-1")
    check[prog.language] = prog.language
    dropDownImg.addEventListener("click", function() {
        switchLanguage(prog.language)
    });
}

function switchLanguage(language) {
    var id = getUserIdFromUrl()
    const url = new URL(window.location.href);
    const newUrl = `${url.origin}/${id}/menu/${language}/Stage1`;
    window.location.href = newUrl;
}

function createStageHeaders(section, keepTrack) {
    var navBar = createNavBarForHeader(keepTrack)
    const mainDiv = document.createElement("div"); 
    const headerDiv = document.createElement("div");
    
    mainDiv.id = `section1${keepTrack.currentStageAlt}`;
    mainDiv.style.display = "none";
    mainDiv.appendChild(headerDiv)

    headerDiv.appendChild(navBar);
    headerDiv.id = `${keepTrack.currentStageAltSmall}Header`;
    headerDiv.classList.add("stage-headers");
    
    // append the main div to the page
    section.appendChild(mainDiv);

    return mainDiv
}

function createStageMainArea(mainDiv, keepTrack) {
    var quizSection = createQuizSection(keepTrack)
    mainDiv.appendChild(quizSection);

    positionButtons()
    
    createDropDownMenuArrows()

    hideAllStages();
    showSelectedStage();

}

function createQuizSection(keepTrack) {
    const row = document.createElement("div")
    row.classList.add("row")

    const quizSection = document.createElement("section");
    quizSection.classList.add("col-12");
    quizSection.id = "quizSection";
    
    const wrapperDiv = document.createElement("div");
    wrapperDiv.classList.add("wrapper")
    wrapperDiv.classList.add("style2")

    const ul = document.createElement("ul");
    ul.classList.add("divided")

    quizSection.appendChild(wrapperDiv)
    wrapperDiv.appendChild(ul)

    level.forEach((lvl) => {
        var li = createButtonForEachLvl(lvl, keepTrack)
        ul.appendChild(li)
    });

    row.appendChild(quizSection)

    return row
}

function createButtonForEachLvl(lvl, keepTrack) {
    const li = document.createElement("li");
    const article = document.createElement("article");

    const button = dropDownButton(lvl, keepTrack);
    const dropdown = dropDownMenu(lvl, keepTrack);
    const chatBubble = createStartBubbleAndJumpBubble(lvl, keepTrack);
    chatBubble.container.appendChild(chatBubble.bubbleStart)
    chatBubble.containerJump.appendChild(chatBubble.bubbleJump)
    article.appendChild(chatBubble.container)
    article.appendChild(chatBubble.containerJump)
    article.appendChild(button)
    article.appendChild(dropdown)

    li.appendChild(article)

    return li
}

function createStartBubbleAndJumpBubble(lvl, keepTrack) {
    var chatBubbles = {
        container: document.createElement("div"),
        containerJump: document.createElement("div"),
        bubbleStart: document.createElement("div"),
        bubbleJump: document.createElement("div"),
    }

    chatBubbles.container.classList.add("chat-bubble-container");
    chatBubbles.container.id = `start${keepTrack.currentStage}Level${lvl}`;
    chatBubbles.container.style.display = 'none';

    chatBubbles.containerJump.classList.add("chat-bubble-container");
    chatBubbles.containerJump.id = `jump${keepTrack.currentStage}Level${lvl}`;
    chatBubbles.containerJump.style.display = 'none';

    chatBubbles.bubbleStart.classList.add("chat-bubble-start")
    chatBubbles.bubbleStart.textContent = "START";

    moveUpDown(chatBubbles.bubbleStart)
    moveUpDown(chatBubbles.bubbleJump)

    chatBubbles.bubbleJump.classList.add("chat-bubble-jump")
    chatBubbles.bubbleJump.textContent = "Jump Here ?";


    return chatBubbles
}

function dropDownButton(lvl, keepTrack) {
    const button = document.createElement("button")
    button.id = `${keepTrack.currentStageSmall}Level${lvl}Button`
    button.name = `${keepTrack.currentStage} ${lvl}`
    button.classList.add("d-btn-lvl", "btn-lvl", "circled", "dropbtn", "scrolly", "margin")
    button.style.marginBottom = "40px";

    const checkMark = document.createElement("div");
    checkMark.classList.add("check-mark"); checkMark.style.display = "none";
    checkMark.id = `check${keepTrack.currentStage}Level${lvl}`
    //checkMark.name = `${keepTrack.currentStage} ${lvl} check`
    button.appendChild(checkMark);

    toggleDrop(button)

    return button
}

function dropDownMenu(lvl, keepTrack) {
    const dropdown = document.createElement("div");
    dropdown.id = "dropDown";
    dropdown.classList.add("dropdown");

    const dropdownContent = document.createElement("div");
    dropdownContent.id = `dc${keepTrack.currentStageSmall}Level${lvl}Button`;
    dropdownContent.classList.add("dropdown-content");

    const h4 = document.createElement("h4");
    h4.classList.add("margin");
    h4.id = `level${lvl}Label`;
    h4.textContent = "Level " + lvl

    const lesson = document.createElement("strong")
    lesson.id = `${keepTrack.currentStage}level${lvl}Lesson`
    lesson.style.marginLeft = "10px";
    lesson.textContent = "Lessons 1 / 3";

    h4.appendChild(lesson)
    dropdownContent.appendChild(h4)
    dropdown.appendChild(dropdownContent)
    dropdown.style.display = "block";

    const button = dropDownMenuButton(lvl, keepTrack)
    dropdownContent.appendChild(button)

    return dropdown

}

function dropDownMenuButton(lvl, keepTrack) {
    const button = document.createElement("button"); 

    button.name = `${keepTrack.currentStage} Level${lvl}`
    button.classList.add("button")
    button.classList.add("level")
    button.classList.add("pushable")
    button.classList.add("margin-top-big")

    var spans = {
        span1: document.createElement("span"),
        span2: document.createElement("span"),
        span3: document.createElement("span"),
        spinner: document.createElement("span"),
    }

    spans.span1.classList.add("d-shadow")
    spans.span2.classList.add("edge")
    spans.span2.id = `lockEdge${keepTrack.currentStage}Level${lvl}`
    spans.span3.classList.add("front-alt")
    spans.span3.id = `lock${keepTrack.currentStage}Level${lvl}`
    spans.span3.textContent = "Start"

    button.appendChild(spans.span1)
    button.appendChild(spans.span2)
    button.appendChild(spans.span3)

    return button
}

function createStageMenu(section, keepTrack) {
    var mainDiv = createStageHeaders(section, keepTrack);
    createStageMainArea(mainDiv, keepTrack)
}


function lockQuestions(button) {
    if (button) {
        button.classList.add("btn-locked");
        button.style.border = "6px solid var(--red)"

        let id = button.id;
        let dropdownContent = document.getElementById("dc" + id);
        if (dropdownContent) {
            dropdownContent.classList.remove("dropdown-content")
            dropdownContent.classList.add("dropdown-content-locked")
        }

    }

}


function unlockQuestions(button) {
    if (button) {
        button.classList.remove("btn-locked");
        button.style.border = "6px solid var(--accent-color-main)"

        let id = button.id;
        let dropdownContent = document.getElementById("dc" + id);
        if (dropdownContent) {
            dropdownContent.classList.add("dropdown-content")
            dropdownContent.classList.remove("dropdown-content-locked")
        }

    }

}

function lockQuestionsDropDown(stageLevel, level) {
    var lock = document.getElementById(`lock${stageLevel}${level}`)
    var lockEdge = document.getElementById(`lockEdge${stageLevel}${level}`)
    if (lock) {
        lock.classList.remove("front-alt")
        lock.classList.add("front-locked")
    }
    if (lockEdge) {
        lockEdge.classList.remove("edge")
        lockEdge.classList.add("edge-lock")
    }

}

function unlockQuestionsDropDown(stageLevel, level) {
    var lock = document.getElementById(`lock${stageLevel}${level}`)
    var lockEdge = document.getElementById(`lockEdge${stageLevel}${level}`)
    if (lock) {
        lock.classList.add("front-alt")
        lock.classList.remove("front-locked")
    }
    if (lockEdge) {
        lockEdge.classList.add("edge")
        lockEdge.classList.remove("edge-lock")
    }

}


function setCurrentProgress() {
    loadCurrentProgress()
}


async function loadCurrentProgress() {
    sendFetchRequest('/get/current/progress', 'GET', null, function(error, responseData) {
        if (error) { // handle errors
            console.error('Error:', error);
        } else { 
            if (responseData) {
                window.history.pushState({}, '', `Stage${responseData}`)
            } else {
                window.history.pushState({}, '', `Stage1`)
            }
        }
    });
}

var isStartRun = 0
function startMainQuiz(cat, level) {
    if (isStartRun == 0) {
        var pathname = window.location.pathname;
        var pathSegments = pathname.split("/");

        // Get the first path segment after the domain
        var id = pathSegments[1];
        isStartRun = 1

        createMainQuiz(id, cat, level).then((data) => {
            const sessionId = data.sentences[0]["id"]

            window.location.href = `/${id}/quiz/${sessionId}`;
            return
        });
    }

    isStartRun++

    if (isStartRun > 4) {
        isStartRun = 0
    }
}

function startSkipToNextStage(stage) {
    var pathname = window.location.pathname;
    var pathSegments = pathname.split("/");

    if (isStartRun == 0) {
        // Get the first path segment after the domain
        var id = pathSegments[1];
        isStartRun = 1

        createSkipSession(id, stage).then((data) => {
            const sessionId = data.sentences[0]["id"]

            window.location.href = `/${id}/quiz/${sessionId}`;
            return
        });
    }

    isStartRun++

    if (isStartRun > 4) {
        isStartRun = 0
    }
}

async function createSkipSession(id, stage) {
    // Create a data object with the body of your POST request
    var data = {
        numberOfQuestions: 20,
    };
    const token = getToken()
    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
        body: JSON.stringify(data),
    };


    return sendFetchUrl(`${apiUrl}/words/api/${id}/aze/skip/stages/${stage}`, requestOptions).then((requestData) => {
        return requestData
    });
}

async function createMainQuiz(id, cat, level) {
    const lesson = document.getElementById(cat+level.toLowerCase()+"Lesson")
    const lessonNumber = lesson.textContent.split(" ")[1]

    // Create a data object with the body of your POST request
    var data = {
        lesson: lessonNumber.trim(),
        numberOfQuestions: 15,
    };
    const token = getToken()
    // Create the request options, including the method, headers, and body
    var requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
        body: JSON.stringify(data),
    };

    return sendFetchUrl(`${apiUrl}/words/api/${id}/aze/${cat}/${level}`, requestOptions).then((requestData) => {
        return requestData
    });
}

// Close the dropdown menu if the user clicks outside of it
function windowClickEvent() {
    window.onclick = function(event) {
        if (!event.target.matches(".dropbtn")) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var dropdownsLocked = document.getElementsByClassName("dropdown-content-locked");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                openDropdown.style.display = "none";
                if (openDropdown.classList.contains("show")) {
                    openDropdown.classList.toggle("show");
                    openDropdown.classList.add("hide");
                }
            }

            for (i = 0; i < dropdownsLocked.length; i++) {
                var openDropdown = dropdownsLocked[i];
                openDropdown.style.display = "none";
                if (openDropdown.classList.contains("show")) {
                    openDropdown.classList.toggle("show");
                    openDropdown.classList.add("hide");
                }
            }
        }

        if (!event.target.matches(".flagbtn") && !event.target.matches(".dropdown-language")) {
            var dropdownsLang = document.getElementsByClassName("dropdown-language");
            var i;
            for (i = 0; i < dropdownsLang.length; i++) {
                var openDropdown = dropdownsLang[i];
                fadeOut(openDropdown)
                check = 0;
            }
        } 
    };
}

document.getElementById("progress").addEventListener("click", function() {
    clearSelectedTab()
    this.classList.add("selected-tab")
    this.style.color = whiteBackground;
    document.getElementById("levels").style.display = "none";
    document.getElementById("subview").style.display = "block";
    slideInFromTop(document.getElementById("subview"))
    document.getElementById("content").style.display = "block";
    document.getElementById("headerTitle").textContent = "Progress";

    window.history.pushState({}, '', `#progress`)
});

document.getElementById("config").addEventListener("click", function() {
    clearSelectedTab()
    this.classList.add("selected-tab")
    this.style.color = whiteBackground;
    document.getElementById("levels").style.display = "none";
    slideInFromTop(document.getElementById("subview"))
    document.getElementById("subview").style.display = "block";
    document.getElementById("content").style.display = "block";
    document.getElementById("headerTitle").textContent = "Configuration";
    
    window.history.pushState({}, '', `#config`)
});

document.getElementById("review1").addEventListener("click", function() {
    document.getElementById("subview").innerHTML = "";
    clearSelectedTab()
    this.classList.add("selected-tab")
    this.style.color = whiteBackground;
    document.getElementById("levels").style.display = "none";
    slideInFromTop(document.getElementById("subview"))
    document.getElementById("subview").style.display = "block";
    document.getElementById("content").style.display = "block";
    document.getElementById("headerTitle").textContent = "Review";
    
    window.history.pushState({}, '', `#review`)
});

document.getElementById("main").addEventListener("click", function() {
    windowClickEvent()

    clearSelectedTab()
    this.classList.add("selected-tab")
    this.style.color = whiteBackground;
    document.getElementById("levels").style.display = "block";
    document.getElementById("content").style.display = "none";
    window.history.pushState({}, '', `Stage1`)
});



function clearSelectedTab() {
    var selectedTabs = document.querySelectorAll(".selected-tab")

    selectedTabs.forEach((selected) => {
        selected.classList.remove("selected-tab")
        selected.style.color = "";
    });
}

function setBackgroundColor(button, amount) {
    gradientColors = [
        `var(--accent-color-main) ${amount}`,
        'var(--alt-background) 0%'
    ];

    button.style.background = `linear-gradient(to top, ${gradientColors.join(', ')})`;
}

function getLargestNumber(listOfNumbers) {
    var largest = 0;
    listOfNumbers.forEach((num) => {
        if (num >= largest) {
            largest = num

        }
    });
    return largest
}


function seperateWord(word) {
    numArray = [1,2,3,4,5,6,7,8,9]
    var txt = word.split("")
    var result = ""

    var result = {
        str: "",
        num: 0,
    }

    txt.forEach((t) => {
        if (numArray.indexOf(parseInt(t)) !== -1) {
            result.num = t
        } else {
            result.str = result.str + t
        }
    });

    return result 
}
