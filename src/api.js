// Url to the api
// Update this to change the main api for the whole project
var apiPort = "3000"
var apiUrl = `http://192.168.0.105:${apiPort}`;

function sendFetchRequest(url, method, data, callback) {
    const token = getToken();
    var requestOptions 
    
    switch (method) {
        case "GET":
            requestOptions = {
                method: "GET",
                headers: {
                    "Content-Type": "application/json", // Specify the content type
                    'Authorization': `${token}`,
                },
            };
        case "POST":
            requestOptions = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json", // Specify the content type
                    'Authorization': `${token}`,
                },
                body: JSON.stringify(data)
            };
    }
    fetch(url, requestOptions)
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(responseData => {
        callback(null, responseData);
    })
    .catch(error => {
        callback(error);
    });
}

async function sendFetchUrl(url, requestOptions) {
    // Make the POST request
    return fetch(`${url}`, requestOptions)
        .then((response) => {
            if (response.status == 400) {
                return response
            }
            if (!response.ok) {
                //throw new Error("Network response was not ok");
                return response
            }
            return response.json(); // Parse the JSON response
        })
        .then((data) => {
            return data;
        })
        .catch((error) => {
            return error
        })
        .finally(() => { });
}

function createRequestOptions(method, data) {
    const token = getToken()
    var requestOptions = {
        method: method,
        headers: {
            'Authorization': `${token}`,
            "Content-Type": "application/json", // Specify the content type
        },
    };

    switch (method) { 
        case "POST":
            requestOptions["body"] = data
    }

    return requestOptions

}

