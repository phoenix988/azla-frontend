function fadeIn(element) {
    element.style.display = 'block';
    element.style.opacity = 0;

    let last = +new Date();
    let tick = function() {
        element.style.opacity = +element.style.opacity + (new Date() - last) / 400;
        last = +new Date();

        if (+element.style.opacity < 1) {
            (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
        }
    };

    tick();
}

function fadeOut(element) {
    element.style.opacity = 1;

    let last = +new Date();
    let tick = function() {
        element.style.opacity = +element.style.opacity - (new Date() - last) / 200;
        last = +new Date();

        if (+element.style.opacity > 0) {
            (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
        } else {
            element.style.display = 'none';
        }
    };

    tick();
}

function moveUpDown(element) {
    let start = null;
    let duration = 4000; // Duration of one full cycle (0% to 100%) in milliseconds
    let initialY = 0; // Initial Y position
    let amplitude = 20; // The distance to move up and down

    function animate(timestamp) {
        if (!start) start = timestamp;
        let progress = timestamp - start;

        // Calculate the progress as a fraction of the duration
        let fraction = (progress % duration) / duration;

        // Calculate the current transform value based on the fraction
        let offsetY = Math.sin(fraction * 2 * Math.PI) * amplitude;

        // Apply the transform to the element
        element.style.transform = `translateY(${initialY + offsetY}px)`;

        // Continue the animation
        requestAnimationFrame(animate);
    }

    requestAnimationFrame(animate);
}

function slideInFromTop(element) {
    let start = null;
    const duration = 200; // Duration of the animation in milliseconds

    function animate(timestamp) {
        if (!start) start = timestamp;
        const progress = timestamp - start;

        // Calculate the progress as a fraction of the duration
        const fraction = Math.min(progress / duration, 1);

        // Interpolate the opacity and transform values
        const opacity = fraction;
        const translateY = (1 - fraction) * -100;

        // Apply the styles to the element
        element.style.opacity = opacity;
        element.style.transform = `translateY(${translateY}%)`;

        // Continue the animation if not complete
        if (fraction < 1) {
            requestAnimationFrame(animate);
        }
    }

    // Initialize the element's styles
    element.style.opacity = 0;
    element.style.transform = 'translateY(-100%)';
    element.style.transition = `transform ${duration}ms, opacity ${duration}ms`; // Ensures smooth transition

    requestAnimationFrame(animate);
}


function slideInFromSide(element) {
    let start = null;
    let duration = 1000; // Duration of the slide-in effect in milliseconds
    let initialX = -200; // Initial X position off-screen
    let finalX = 0; // Final X position on-screen
    let initialY = 100; // Initial Y position off-screen

    function animate(timestamp) {
        if (!start) start = timestamp;
        let progress = timestamp - start;

        // Calculate the progress as a fraction of the duration
        let fraction = Math.min(progress / duration, 1);

        // Calculate the current transform values based on the fraction
        let offsetX = initialX + fraction * (finalX - initialX);
        let offsetY = initialY + fraction * (-200 - initialY);

        // Apply the transform to the element
        element.style.transform = `translateX(${offsetX}px) translateY(${offsetY}px)`;

        // Continue the animation if it has not yet completed
        if (fraction < 1) {
            requestAnimationFrame(animate);
        }
    }

    requestAnimationFrame(animate);
}
