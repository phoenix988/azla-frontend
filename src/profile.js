function validateInput() {
    var id = getUserIdFromUrl()
    
    getUserProfileById(id).then((profile) => {
        var currentEmail = profile.email
        var currentUsername = profile.username
        validateEmail(currentEmail);

        validateUsername(currentUsername)
    });

    validatePassword()
}

function createSelectAllEvent() {
    var selectAllButton = document.getElementById("selectAll")
    var selectAll = document.querySelectorAll(".lang-check")
    
    if ( selectAllButton ) {
        selectAllButton.addEventListener("click", function() {
            selectAll.forEach((select) => {
                if (select.checked) {
                    select.checked = false;
                    selectAllButton.textContent = "Select All"
                    return
                }

                select.checked = true;
                selectAllButton.textContent = "Deselect All"
            });

        });
    }
}

function clearSelectedSetting(user) {
    for (var key in user) {
        user[key].classList.remove("user-container")
    }
}

function uploadImage() {
    var form = document.getElementById('uploadForm');
    var formData = new FormData(form);
    
    fetch('/upload/image_temp', {
        method: 'POST',
        body: formData
    })
    .then(response => response.text())
    .then(data => {
    })
    .catch((error) => {
        console.error('Error:', error);
        alert('Error uploading file');
    });
}

var isRun = 0;
var keepTrack = 0
document.addEventListener("htmx:afterSettle", function(event) {
    if (document.getElementById('file-input')) {
        document.getElementById('file-input').addEventListener('change', function() {
            var fileName = this.files[0].name;
            document.getElementById('file-name').textContent = fileName;
            uploadImage()
            //loadProfilePictureTemp(`user_data/temp/${fileName}`)
        });
    }
    createSelectAllEvent()

    validateInput();

    activateDarkModeSwitch()
    
    loadProfilePicture()

    var closeButton = document.getElementById("closeButton")
    var yesButton = document.getElementById("yesButton")
    if (closeButton) {
        closeButton.addEventListener("click", function() {
            document.getElementById("message").innerHTML = "";
            var popup = document.getElementById("popupMessage");
            if (popup) {
                popup.remove();
            }
        });
    }

    if (yesButton) {
    }
    
    var addMoreLanguages = document.getElementById("addMoreLang") 
    if (addMoreLanguages) {
        document.getElementById("addMoreLang").addEventListener("click", function() {});
    }

});



function addMoreLanguages() {
    if (keepTrack == 0) {
        slideInFromTop(document.getElementById("configuration"))
        document.getElementById("configuration").style.display = "block"
        keepTrack = 1 
    }

    else if (keepTrack == 1) {
        document.getElementById("configuration").style.display = "none"
        keepTrack = 0
    }
}

function pictureClickAction(img) {
    var profilePicture = document.getElementById("profilePicture")
    profilePicture.src = img.src

    let id = getUserIdFromUrl();

    let imgPath = img.src

    updateProfilePicture(imgPath.replace(/^https?:\/\/[^\/]+/, '.'))
}

var isShown = 0
function showAllPictures() {
    if (isShown == 0) {
        var requestOptions = createRequestOptions("GET") 
        document.getElementById("loadingScreen").style.display = "block";
        document.getElementById("spinner").style.opacity = "1";
        sendFetchUrl("/list/images", requestOptions).then((data) => {
            var container = document.getElementById("allPictures")

            slideInFromTop(container)
            container.classList.add("container", "list-of-pictures")
            var id = getCookie("id")
            var promises = [];
            for (i = 0; i < data.length; i++ ) {
                var img = document.createElement("img")
                var col = document.createElement("div")
                var a = document.createElement("a")
                col.classList.add("col-4")
                a.classList.add("image", "fit")
                img.src = `/user_data/${id}/${data[i]}`
                img.style.padding = "5px";
                container.appendChild(col)
                col.appendChild(a)
                a.appendChild(img)
                img.classList.add("profile-pictures")

                // Create promise for each image load
                var imageLoadPromise = new Promise(function(resolve) {
                    img.onload = resolve;
                });
                promises.push(imageLoadPromise);
            }
            Promise.all(promises).then(function() {
                document.getElementById("loadingScreen").style.display = "none"; // Hide loading screen

                var pictures = document.querySelectorAll(".profile-pictures")
                pictures.forEach((pic) => {
                    pic.addEventListener("click", function()  {
                        // Highlight selected picture
                        var pictures = document.querySelectorAll(".profile-pictures")
                        pictures.forEach((pic) => {
                            pic.classList.remove("selected-tab")
                        });

                        this.classList.add("selected-tab")

                        pictureClickAction(pic)
                    });

                });

                document.getElementById("spinner").style.opacity = "0";
            });
        })

        isShown = 1
    } else {
        document.getElementById("allPictures").innerHTML = "";
        isShown = 0
    }

}


document.addEventListener("DOMContentLoaded", function() {
    validateInput();

    activateDarkModeSwitch()

    createSelectAllEvent()

    var user = {
        Language: document.getElementById("userContainerLanguage"),
        Info: document.getElementById("userContainerInfo"),
        Sec: document.getElementById("userContainerSec"),
        //Progression: document.getElementById("userContainerProgression"),
    }


    for (var key in user) {
        user[key].addEventListener("click", function() {
            clearSelectedSetting(user)
            this.classList.add("user-container")
            window.location.href = "#wheel"
        });

    }


});


// validate on submit
(function ($) {
    "use strict";

    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    // Validate on blur event
    input.each(function(){
        $(this).on('blur', function(){
            validate(this);
        });
    });

    function validate(input) {
        if($(input).attr('type') === 'email' || $(input).attr('name') === 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) === null) {
                showValidate(input);
            } else {
                hideValidate(input);
            }
        }
        else {
            if($(input).val().trim() === ''){
                showValidate(input);
            } else {
                hideValidate(input);
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();
        $(thisAlert).removeClass('alert-validate');
    }

})(jQuery);

(function ($) {
    "use strict";

    
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }
        
        createUser(event)

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    
    

})(jQuery);

