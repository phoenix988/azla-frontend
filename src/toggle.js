function toggleDarkMode() {
    document.documentElement.classList.toggle("dark-mode")
}

function loadDarkMode() {
    fetchSettings().then((settings) => {
        if (settings.darkmode) {
            document.documentElement.classList.add("dark-mode")
        } else {
            document.documentElement.classList.remove("dark-mode")
        }
    });
}

async function fetchSettings() {
    let id = getUserIdFromUrl();
    
    var requestOptions = createRequestOptions("GET")

    return sendFetchUrl(`${apiUrl}/account/${id}/settings`, requestOptions).then((requestData) => {
        return requestData
    });
}


function updateSettingForAccount(setting, action) {
    const id = getUserIdFromUrl();
    var data = {};

    var requestOptions = createRequestOptions("POST", data)

    sendFetchUrl(`${apiUrl}/account/${id}/settings/${setting}/${action}`, requestOptions).then(() => {});
}


function activateDarkModeSwitch() {
    const themeSwitch = document.getElementById("darkModeSwitch");
    if (themeSwitch) {
        themeSwitch.addEventListener("change", function(event) {
            updateSettingForAccount("dark_mode", themeSwitch.checked)
        });

        fetchSettings().then((settings) => {
            if (settings.darkmode) {
                themeSwitch.checked = true;
            } else {
                themeSwitch.checked = false;

            }
        });

    }
}

function loadProfilePicture() {
    var profilePictures = document.querySelectorAll(".pfp");
    profilePictures.forEach((pfp) => {
        if (pfp) {
            fetchSettings().then((data) => {
                if (!data.profilePicture) {
                    pfp.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Default_pfp.svg/340px-Default_pfp.svg.png";
                } else {
                    var split = data.profilePicture.split("/");
                    pfp.src = `/user_data/${split[2]}/${split[3]}`;
                    pfp.onerror = function() {
                        pfp.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Default_pfp.svg/340px-Default_pfp.svg.png";
                    };
                }
            });
        }
    });
}

function loadProfilePictureTemp(picture) {
    var profilePictures = document.querySelectorAll(".pfp")
    profilePictures.forEach((pfp) => {
        var path = picture.replace(/ /g, '_')
        pfp.src = `${path}`;
    });

}

function updateProfilePicture(picture) {
    const token = getToken();
    var data = {
        profilePicture: picture,
    };

    var requestOptions = {
        method: "POST",
        headers: {
            "Content-Type": "application/json", // Specify the content type
            'Authorization': `${token}`,
        },
        body: JSON.stringify(data)
    };

    let id = getUserIdFromUrl();

    return sendFetchUrl(`${apiUrl}/account/${id}/settings/picture/update`, requestOptions).then((requestData) => {
        return requestData
    }).catch((error) => {
        return error
    });

}

async function fileExists(url) {
    const response = await fetch(url, { method: 'HEAD' });
    return response.status !== 404;
}

document.addEventListener("DOMContentLoaded", loadDarkMode())
document.addEventListener("DOMContentLoaded", loadProfilePicture())
