package main

import (
    "net/http"
    "io"
    "encoding/json"
    "strings"
    "bytes"
)

type Requests interface {
    // Get requests
    GetUserProfile(string, string) (map[string]interface{}, error)
    GetUserProfileByID(string, *http.Request) (map[string]interface{}, error)
    GetUserSessionData(string, string, string) (map[string]interface{}, error)
    GetAllSessionsForUser(string, string) ([]map[string]interface{}, error)
    GetImageForQuestion(string) (string, error)
    GetAllProgress(string, *http.Request) ([]map[string]interface{}, error)
    GetProgress(string, string) (map[string]interface{}, error)
    GetProgressByLang(string, string, *http.Request) (map[string]interface{}, error)
    GetLanguages() ([]map[string]interface{}, error)
    GetLanguageByName(string) (map[string]interface{}, error)
    GetSettings(string, string) (map[string]interface{}, error)
    
    // Update requests
    UpdateSettings(string, string, string, map[string]string) (map[string]interface{}, error)
    UpdateAccountSettings(*http.Request, string, string) (map[string]interface{}, error)
    UpdateAccountProfilePicture(*http.Request, string, string) (map[string]interface{}, error)
    CreateAccountSettings(*http.Request) (map[string]interface{}, error)

    // User auth/create user
    CreateUser(*CreateUserRequest) (map[string]interface{})
    Authenticate(string,string) (map[string]interface{}, error)

    // Session requests
    StartNewReviewSession(string, string,map[string]interface{}) (map[string]interface{}, error)
    //StartNewSession(*NewSessionRequest) (map[string]interface{})
    //GetCurrentWords(*CurrentWordRequest) (map[string]interface{}, error)
    //EvaluateQuiz(*CurrentWordRequest) (map[string]interface{}, error)
    //GetSessionAnswers(string, string) ([]interface{}, error)
    //UpdateAnswer(string, string, string, string) (map[string]interface{})
    //UpdateQuestion(string, string, string) (map[string]interface{}, error)
    
    // check requests
    CheckIfEmailIsOk(string) (bool, error)
    CheckIfUserNameIsOk(string) (bool, error)

    DeleteProgress(string, string, string) (interface{}, error)
    ResetProgress(string, string, string) (interface{}, error)

    UpdateSessionComplete(*http.Request, string) (interface{}, error)
}

type Send interface {
    InterFace([]byte) (interface{}, error)
    InterFaceMap([]byte) (map[string]interface{}, error)
    Bool([]byte) (bool, error)
    Slice([]byte) ([]map[string]interface{}, error)
    Str([]byte) (string, error)
}


type ReturnSend interface {
    InterFace(string, string, string, map[string]string) (interface{}, error)
    Slice(string, string, string, map[string]string) ([]map[string]interface{}, error)
}

type SendRequests struct{}

type ReturnRequests struct{}


func NewSendMethods() *Sending {
	return &Sending{
		Send: &SendRequests{},
		ReturnRequests: &ReturnRequests{},
	}
}

var send = NewSendMethods()

type ApiUrl struct {
	url string
}

// url to backend server
var url = "http://localhost:3000"

func NewRequests() (*ApiUrl, error) {
    return &ApiUrl {
        url: url,
    }, nil
}



func(s *ApiUrl) GetLanguageByName(lang string) (map[string]interface{}, error) {
    language, err := SendRequest(s.url+"/get/language/"+lang, "GET", "", nil)
    if err != nil {
        return nil, err
    }
    
    
    return language, nil
}

func(s *ApiUrl) GetLanguages() ([]map[string]interface{}, error) {
    languages, err := SendRequestSlice(s.url+"/get/language", "GET", "", nil)
    if err != nil {
        return nil, err
    }
    
    
    return languages, nil
}

func(s *ApiUrl) UpdateSessionComplete(r *http.Request, sessionId string) (interface{}, error) {
    id := getCookie(r, "id")
    token, err := getToken(r)
    if err != nil {
        return nil, err
    }
    sessions, err := SendRequestInterface(s.url+"/account/"+id+"/sessions/update/"+sessionId, "GET", token, nil)
    
    return sessions, nil
}

func(s *ApiUrl) GetAllSessionsForUser(id, token string) ([]map[string]interface{}, error) {
    sessions, err := SendRequestSlice(s.url+"/account/"+id+"/sessions", "GET", token, nil)
    if err != nil {
        return nil, err
    }
    
    return sessions, nil
}


func(s *ApiUrl) CheckIfLevelIsUnlocked(id, token string) ([]map[string]interface{}, error) {
    return nil, nil
}


func(s *ApiUrl) GetImageForQuestion(word string) (string, error) {
    url, err := SendRequestStr(s.url+"/words/api/search/"+word, "GET", "", nil)
    if err != nil {
        return "", err
    }

    return url, nil
}


func(s *ApiUrl) GetProgressByLang(id, lang string, r *http.Request) (map[string]interface{}, error) {
    token, _ := getToken(r)
    progress, err := SendRequest(s.url+"/progress/api/"+id+"/"+lang, "GET", token, nil)

    return progress, err
}


func(s *ApiUrl) GetProgress(id, token string) (map[string]interface{}, error) {
    settings, _ := s.GetSettings(id, token)

    language, _ := settings["defaultLanguage"].(string)
    if language == "" {
        language = "azerbajani"
    }
    
    language = strings.TrimSpace(language)
    progress, err := SendRequest(s.url+"/progress/api/"+id+"/"+language, "GET", token, nil)


    return progress, err
}


func(s *ApiUrl) GetAllProgress(id string, r *http.Request) ([]map[string]interface{}, error) {
    token, err := getToken(r)

    if err != nil {
        return nil, err
    }
    progress, err := SendRequestSlice(s.url+"/progress/api/"+id+"/get/all", "GET", token, nil)

    return progress, err
}


func(s *ApiUrl) UpdateSettings(id, token, action string, data map[string]string) (map[string]interface{}, error) {
    settings, err := SendRequest(s.url+"/account/"+id+"/update/"+action, "POST", token, data)

    return settings, err
}


func(s *ApiUrl) UpdateAccountSettings(r *http.Request, action, attribute string) (map[string]interface{}, error) {
    id := getCookie(r, "id")
    token, _ := getToken(r)
    settings, err := SendRequest(s.url+"/account/"+id+"/settings/"+action+"/"+attribute, "POST", token, nil)

    if err != nil {
        return nil, err
    }

    return settings, err
}


func(s *ApiUrl) UpdateAccountProfilePicture(r *http.Request, action, attribute string) (map[string]interface{}, error) {
    id := getCookie(r, "id")
    token, _ := getToken(r)
    data := map[string]string{"profilePicture": attribute}
    settings, err := SendRequest(s.url+"/account/"+id+"/settings/"+action+"/update", "POST", token, data)

    if err != nil {
        return nil, err
    }

    return settings, err
}


func(s *ApiUrl) CreateAccountSettings(r *http.Request) (map[string]interface{}, error) {
    id := getCookie(r, "id")
    token, _ := getToken(r)
    settings, err := SendRequest(s.url+"/account/"+id+"/settings", "POST", token, nil)

    if err != nil {
        return nil, err
    }

    return settings, err
}


func(s *ApiUrl) GetSettings(id, token string) (map[string]interface{}, error) {
    settings, err := SendRequest(s.url+"/account/"+id+"/settings", "GET", token, nil)

    return settings, err
}

func(s *ApiUrl) StartNewReviewSession(id, token string, data map[string]interface{}) (map[string]interface{}, error) {
    newSessions, err := SendRequestInterface(s.url+"/words/api/"+id+"/aze/custom/review/mistake","POST", token, data)
    if err != nil {
        return nil, err
    }
    
    return newSessions, nil
}


func(s *ApiUrl) DeleteProgress(id, language, token string) (interface{}, error) {
    body, err := Request(s.url+"/progress/api/"+id+"/"+language, token, "DELETE", nil)
    if err != nil {
        return nil, err
    }

    return send.Send.InterFace(body)
}


func(s *ApiUrl) ResetProgress(id, language, token string) (interface{}, error) {
    body, err := Request(s.url+"/progress/api/"+id+"/"+language, token, "PUT", nil)
    if err != nil {
        return nil, err
    }

    return send.Send.InterFace(body)
}


func(s *ApiUrl) CheckIfEmailIsOk(email string) (bool, error) {
    checkEmail, err := SendRequestBool(s.url+"/email/check/"+email, "GET", "", nil)
    if err != nil {
        return false, err
    }
    
    return checkEmail, nil
}


func(s *ApiUrl) CheckIfUserNameIsOk(username string) (bool, error) {
    checkUser, err := SendRequestBool(s.url+"/user/"+username, "GET", "", nil)
    if err != nil {
        return false, err
    }
    
    return checkUser, nil
}


func(r *ReturnRequests) InterFace(url, method, token string, requestData map[string]string) (interface{}, error) {
    body, err := Request(url, token, method, requestData)
    if err != nil {
        return nil, err
    }

    return send.Send.InterFaceMap(body)
}

func SendRequest(url, method, token string, requestData map[string]string) (map[string]interface{}, error) {
    body, err := Request(url, token, method, requestData)
    if err != nil {
        return nil, err
    }

    return send.Send.InterFaceMap(body)
}


func SendRequestInterface(url, method, token string, requestData map[string]interface{}) (map[string]interface{}, error) {
    body, err := RequestInterface(url, token, method, requestData)
    if err != nil {
        return nil, err
    }

    return send.Send.InterFaceMap(body)
}


func(r *ReturnRequests) Slice(url, method, token string, requestData map[string]string) ([]map[string]interface{}, error) {
    body, err := Request(url, token, method, requestData)
    if err != nil {
        return nil, err
    }

    return send.Send.Slice(body)
}



func SendRequestStr(url, method, token string, requestData map[string]string) (string, error) {
    body, err := Request(url, token, method, requestData)
    if err != nil {
        return "", err
    }
    
    return send.Send.Str(body)
}   


func SendRequestSlice(url, method, token string, requestData map[string]string) ([]map[string]interface{}, error) {
    body, err := Request(url, token, method, requestData)
    if err != nil {
        return nil, err
    }

    return send.Send.Slice(body)
}


func SendRequestBool(url, method, token string, requestData map[string]string) (bool, error) {
    body, err := Request(url, token, method, requestData)
    if err != nil {
        return false, err
    }


    return send.Send.Bool(body)
}



func(s *SendRequests) Str(body []byte) (string, error) {
    // Handle the response as needed
    var response string
    err := json.Unmarshal(body, &response)
    if err != nil {
        return "", err
    }

    return response, nil
    
    
}


func(s *SendRequests) InterFace(body []byte) (interface{}, error) {
    // Handle the response as needed
    var response interface{}
    err := json.Unmarshal(body, &response)
    if err != nil {
        return nil, err
    }

    return response, nil
}


func(s *SendRequests) InterFaceMap(body []byte) (map[string]interface{}, error) {
    // Handle the response as needed
    var response map[string]interface{}
    err := json.Unmarshal(body, &response)
    if err != nil {
        return nil, err
    }

    return response, nil

}

func(s *SendRequests) Bool(body []byte) (bool, error) {
    // Handle the response as needed
    var response bool
    err := json.Unmarshal(body, &response)
    if err != nil {
        return false, err
    }

    return response, nil
}


func(s *SendRequests) Slice(body []byte) ([]map[string]interface{}, error) {
    // Handle the response as needed
    var response []map[string]interface{}
    err := json.Unmarshal(body, &response)
    if err != nil {
        return nil, err
    }

    return response, nil
}




func RequestInterface(url, token, method string, requestData map[string]interface{}) ([]byte, error){
    client := &http.Client{}

    // Encode JSON data
    jsonData, err := json.Marshal(requestData)
    if err != nil {
        return nil, err
    }

    // Create a new GET request with the URL
    req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonData))
    if err != nil {
        return nil, err
    }

    // Set the Authorization header
    req.Header.Set("Authorization", token)

    // Send the request
    resp, err := client.Do(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Read the response body
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }


    return body, nil
}

func Request(url, token, method string, requestData map[string]string) ([]byte, error){
    client := &http.Client{}

    // Encode JSON data
    jsonData, err := json.Marshal(requestData)
    if err != nil {
        return nil, err
    }

    // Create a new GET request with the URL
    req, err := http.NewRequest(method, url, bytes.NewBuffer(jsonData))
    if err != nil {
        return nil, err
    }

    // Set the Authorization header
    req.Header.Set("Authorization", token)

    // Send the request
    resp, err := client.Do(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    // Read the response body
    body, err := io.ReadAll(resp.Body)
    if err != nil {
        return nil, err
    }


    return body, nil
}


