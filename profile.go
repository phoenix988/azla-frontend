package main

import (
	"github.com/gorilla/sessions"
	"net/http"
	"strconv"
)

var store = sessions.NewCookieStore([]byte("secret-key"))

func loginUser(s *WebServer, username, password string) (map[string]interface{}, error) {
	attempt, err := s.requests.Authenticate(username, password)

	if err != nil {
		return nil, err
	}

	return attempt, nil

}

func (s *ApiUrl) Authenticate(username, password string) (map[string]interface{}, error) {
	// Define the JSON payload
	data := map[string]string{
		"userName": username,
		"password": password,
	}

	isSuccess, err := SendRequest(s.url+"/login", "POST", "", data)

	if err != nil {
		return nil, err
	}

	return isSuccess, nil
}

func (s *ApiUrl) GetUserProfile(email, token string) (map[string]interface{}, error) {
	if email != "" {

		profile, err := SendRequest(s.url+"/email/"+email, "GET", token, nil)

		if err != nil {
			return nil, err
		}

		return profile, nil
	}

	return nil, nil

}

func (s *ApiUrl) GetUserProfileByID(id string, r *http.Request) (map[string]interface{}, error) {
	url := url + "/account/" + id
	token, err := getToken(r)

	if err != nil {
		return nil, err
	}
	profile, err := SendRequest(url, "GET", token, nil)
	if err != nil {
		return nil, err
	}

	return profile, nil

}

func (s *ApiUrl) GetUserSessionData(id, sessionID, token string) (map[string]interface{}, error) {
	url := url + "/account/" + id + "/sessions/" + sessionID
	profile, err := SendRequest(url, "GET", token, nil)
	if err != nil {
		return nil, err
	}

	return profile, nil

}

func (s *ApiUrl) CreateUser(createUser *CreateUserRequest) map[string]interface{} {
	// Define the JSON payload
	data := map[string]string{
		"userName":        createUser.Username,
		"password":        createUser.Password,
		"confirmPassword": createUser.PasswordConfirm,
		"email":           createUser.Email,
		"firstName":       createUser.FirstName,
		"lastName":        createUser.LastName,
	}

	newAccount, err := SendRequest(url+"/account", "POST", "", data)
	if err != nil {
		return nil
	}

	return newAccount

}

func (s *ApiUrl) evaluateAnswer(answer, id string) string {
	// Define the JSON payload
	data := map[string]string{
		"answer": answer,
	}

	result, err := SendRequestStr(s.url+"/words/api/eval_drag/"+id, "POST", "", data)
	if err != nil {
		return ""
	}

	return result
}

func saveSession(w http.ResponseWriter, r *http.Request, id, username string) {
	session, _ := store.Get(r, "session-name")
	userID, _ := strconv.Atoi(id)

	session.Values["user_id"] = userID
	session.Values["username"] = username

	session.Save(r, w)
}

func removeSession(w http.ResponseWriter, r *http.Request) {
	session, _ := store.Get(r, "session-name")

	// Revoke authentication
	delete(session.Values, "user_id")
	delete(session.Values, "username")
	session.Save(r, w)

	http.Redirect(w, r, "/", http.StatusFound)
}
