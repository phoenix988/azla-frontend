package main


import (
)

type Update interface {
	Email(s *WebServer, id, token, email string) (bool)
	UserName(s *WebServer, id, token, username string)
	FullName(s *WebServer, id, token string, fullname []string)
}

func NewAccountMethods() *Accounts {
	return &Accounts{
		Update: &Updates{},
	}
}

// CustomUpdate struct that implements the Update interface
type Updates struct{}

var account = NewAccountMethods()

func (u *Updates) Email(s *WebServer, id, token, email string) bool {
	doesEmailExist, _ := s.requests.CheckIfEmailIsOk(email)

	if doesEmailExist {
		return true
	}
	
	requestBody := map[string]string{"email": email}
	s.requests.UpdateSettings(id, token, "email", requestBody)

	return false
}


func (u *Updates) FullName(s *WebServer, id,token string, fullname []string) {
	if fullname[0] != "" && fullname[1] != "" {
		requestBody := map[string]string{"firstname": fullname[0], "lastname": fullname[1]}
		s.requests.UpdateSettings(id, token, "name", requestBody)
	}
}


func (u *Updates) UserName(s *WebServer, id,token string, username string) {
		requestBody := map[string]string{"username": username}
		s.requests.UpdateSettings(id, token, "username", requestBody)
}


